#ifndef KERNEL_H_
#define KERNEL_H_

typedef struct {
    int errnum; /* error number */
    char errmess[252]; /* error message (zero terminated) */
} _kernel_oserror;

typedef struct {
    int r[10]; /* only r0 - r9 matter for swi's */
} _kernel_swi_regs;

#define _kernel_NONX 0x80000000

_kernel_oserror* _kernel_swi(int no, _kernel_swi_regs* in, _kernel_swi_regs* out);
_kernel_oserror* _kernel_swi_c(int no, _kernel_swi_regs* in, _kernel_swi_regs* out, int* carry);

#endif // KERNEL_H_