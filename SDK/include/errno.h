#ifndef ERRNO_H_
#define ERRNO_H_

#define EDOM 1
#define ERANGE 2
#define EILSEQ 3
extern int errno;

#endif // ERRNO_H_