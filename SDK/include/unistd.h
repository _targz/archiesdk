#ifndef UNISTD_H_
#define UNISTD_H_

#include <sys/types.h>
#include <stdint.h>

//TBD, signatures here to allow libgcc/libgcov to build with libc support

pid_t fork(void);
int execv(const char* path, char* const argv[]);
int execve(const char* path, char* const argv[], char* const envp[]);
int execvp(const char* file, char* const argv[]);
pid_t getpid(void);

#endif // UNISTD_H_