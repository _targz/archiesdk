#ifndef STRING_H_
#define STRING_H_

#include <decls/size_t.h>
#include <decls/NULL.h>

void* memcpy(void* dest, const void* src, size_t size);
void* memset(void* dest, const int c, size_t size);
int memcmp(const char* str1, const char* str2, size_t n);
void* memchr(const void* str, int c, size_t n);

char* strcpy(char* dest, const char* src);
char* strncpy(char* dest, const char* src, size_t n);
char* strcat(char* dest, const char* src);
char* strncat(char* dest, const char* src, size_t n);
size_t strlen(const char* string);
size_t strnlen(const char* string, size_t n);
int strcoll(const char* str1, const char* str2);
int strcmp(const char* str1, const char* str2);
int strncmp(const char* str1, const char* str2, size_t n);
char* strchr(const char* str, int c);
char* strrchr(const char* str, int c);
size_t strspn(const char* str1, const char* str2);
size_t strcspn(const char* str1, const char* str2);
char* strpbrk(const char* str1, const char* str2);
char* strstr(const char* haystack, const char* needle);
char* strtok(char* str, const char* delim);
char* strerror(int errnum);

#endif // STRING_H_