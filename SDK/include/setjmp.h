#ifndef SETJMP_H_
#define SETJMP_H_

typedef struct {
    int* gccBuffer[5];
} __jmp_buf_tag;

typedef __jmp_buf_tag jmp_buf[1];

#define setjmp(env) __builtin_setjmp(env)

#define longjmp(environment, value) __builtin_longjmp(environment, value)

#endif // SETJMP_H_