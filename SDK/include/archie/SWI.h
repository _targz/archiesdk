#ifndef SWI_H
#define SWI_H

#include "swis.h"

#define OSByte_EventEnable 14
#define OSByte_EventDisable 13
#define OSByte_Vsync 19
#define OSByte_WriteVDUBank 112
#define OSByte_WriteDisplayBank 113
#define OSByte_ReadKey 129

#define OSWord_WritePalette 12

#define ErrorV 0x01
#define EventV 0x10
#define Event_VSync 4

#define VD_ScreenStart 148

#define swi_postExp(x) asm("SWI "#x : : : "cc", "memory")
#define swi(x) swi_postExp(x)

#define swiToConstx(x) "#"#x
#define swiToConst(x) swiToConstx(x)
#endif
