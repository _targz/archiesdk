#ifndef VIDEO_H
#define VIDEO_H

#include "archie/SDKTypes.h"

void v_enableVSync(void);
void v_disableVSync(void);
void v_waitForVSync(void);

void* v_getScreenAddress(void);

void v_setMode(u32 mode);
void v_disableTextCursor(void);

void v_setBorderColourRGB(u8 r, u8 g, u8 b);
void v_setBorderColour(u32 colour);

#endif