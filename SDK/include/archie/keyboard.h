#ifndef ARCHIE_KEYBOARD_H_
#define ARCHIE_KEYBOARD_H_

#include <stdbool.h>
#include "archie/SDKTypes.h"

#define KEY_SHIFT 0
#define KEY_CTRL 1
#define KEY_ALT 2

//left side special keys
#define KEY_LSHIFT 3
#define KEY_LCTRL 4
#define KEY_LALT 5

//right side special keys
#define KEY_RSHIFT 6
#define KEY_RCTRL 7
#define KEY_RALT 8

//left mouse button
#define KEY_LMOUSE 9
#define KEY_LEFTCLICK 9
//middle mouse button
#define KEY_MMOUSE 10
#define KEY_MIDDLECLICK 10
//right mouse button
#define KEY_RMOUSE 11
#define KEY_RIGHTCLICK 11

#define KEY_DASH 23
#define KEY_HYPHEN 23
#define KEY_MINUS 23
#define KEY_LEFT 25
#define KEY_SCROLLLOCK 31
#define KEY_PRINTSCREEN 32
#define KEY_DOWN 41
#define KEY_BREAK 44
#define KEY_TILDE 45
#define KEY_POUND 46
#define KEY_BACKSPACE 47
#define KEY_SQUAREBRACKETSTART 56
#define KEY_UP 57
#define KEY_INSERT 61
#define KEY_HOME 62
#define KEY_PAGEUP 63
#define KEY_CAPS 64
#define KEY_RETURN 73
#define KEY_NUMLOCK 77
#define KEY_PAGEDOWN 78
#define KEY_DOUBLEQUOTES 79
#define KEY_SEMICOLON 87
#define KEY_SQUAREBRACKETEND 88
#define KEY_DELETE 89
#define KEY_PLUS 93
#define KEY_EXTRA 94
#define KEY_TAB 96
#define KEY_SPACE 98
#define KEY_COMMA 102
#define KEY_PERIOD 103
#define KEY_SLASH 104
#define KEY_END 105
#define KEY_COPY 105
#define KEY_ESC 112
#define KEY_ESCAPE 112
#define KEY_BACKSLASH 120
#define KEY_RIGHT 121
#define KEY_LEFTLOGO 125
#define KEY_RIGHTLOGO 126
#define KEY_MENU 127

//letters
#define KEY_Q 16
#define KEY_W 33
#define KEY_E 34
#define KEY_T 35
#define KEY_I 37
#define KEY_D 50
#define KEY_R 51
#define KEY_6 52
#define KEY_U 53
#define KEY_O 54
#define KEY_P 55
#define KEY_A 65
#define KEY_X 66
#define KEY_F 67
#define KEY_Y 68
#define KEY_J 69
#define KEY_K 70
#define KEY_S 81
#define KEY_C 82
#define KEY_G 83
#define KEY_H 84
#define KEY_N 85
#define KEY_L 86
#define KEY_Z 97
#define KEY_V 99
#define KEY_B 100
#define KEY_M 101

//numbers
#define KEY_3 17
#define KEY_4 18
#define KEY_5 19
#define KEY_8 21
#define KEY_7 36
#define KEY_9 38
#define KEY_0 39
#define KEY_1 48
#define KEY_2 49

//function keys
#define KEY_F4 20
#define KEY_F7 22
#define KEY_F11 28
#define KEY_F12 29
#define KEY_F10 30
#define KEY_F1 113
#define KEY_F2 114
#define KEY_F3 115
#define KEY_F5 116
#define KEY_F6 117
#define KEY_F8 118
#define KEY_F9 119

//keypad keys
#define KEY_KP6 26
#define KEY_KP7 27
#define KEY_KP8 42
#define KEY_KP9 43
#define KEY_KPPLUS 58
#define KEY_KPMINUS 59
#define KEY_KPENTER 60
#define KEY_KPSLASH 74
#define KEY_KPPERIOD 76
#define KEY_KPHASH 90
#define KEY_KPASTERISK 91
#define KEY_KP0 106
#define KEY_KP1 107
#define KEY_KP3 108
#define KEY_KP4 122
#define KEY_KP5 123
#define KEY_KP2 124

bool k_checkKeypress(u32 key);

#endif // ARCHIE_KEYBOARD_H_