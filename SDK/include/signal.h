#ifndef SIGNAL_H_
#define SIGNAL_H_

typedef void (*__sighandler_t) (int);
typedef int __pid_t;
typedef int sig_atomic_t;

#define SIGTERM 1
#define SIGSEGV 2
#define SIGINT 3
#define SIGILL 4
#define SIGABRT 5
#define SIGFPE 6
#define SIG_DFL ((__sighandler_t)0)
#define SIG_IGN ((__sighandler_t)0)

__sighandler_t signal(int sig, __sighandler_t func);
int kill(__pid_t pid, int sig);
int raise(int sig);

#endif