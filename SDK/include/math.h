#ifndef MATH_H_
#define MATH_H_

#define HUGE_VALF (__builtin_huge_valf())
#define HUGE_VALL (__builtin_huge_vall())
#define NAN (__builtin_nanf(""))
#define INFINITY (__builtin_inff())

#define M_E		    2.7182818284590452354f	// e
#define M_LOG2E	    1.4426950408889634074f	// log_2 e
#define M_LOG10E	0.43429448190325182765f	// log_10 e
#define M_LN2		0.69314718055994530942f	// log_e 2
#define M_LN10		2.30258509299404568402f	// log_e 10
#define M_PI		3.14159265358979323846f	// pi
#define M_PI_2		1.57079632679489661923f	// pi/2
#define M_PI_4		0.78539816339744830962f	// pi/4
#define M_1_PI		0.31830988618379067154f	// 1/pi
#define M_2_PI		0.63661977236758134308f	// 2/pi
#define M_2_SQRTPI	1.12837916709551257390f	// 2/sqrt(pi)
#define M_SQRT2	    1.41421356237309504880f	// sqrt(2)
#define M_SQRT1_2	0.70710678118654752440f	// 1/sqrt(2)

#define isnan(x) __builtin_isnan(x)
#define isnormal(x) __builtin_isnormal(x)
#define isfinite(x) __builtin_isfinite(x)
#define isinf(x) __builtin_isinf_sign(x)
#define signbit(x) __builtin_signbit(x)
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

double acos(double x);
double asin(double x);
double atan(double x);
double atan2(double y, double x);
double cos(double x);
double sin(double x);
double tan(double x);

double cosh(double x);
double sinh(double x);
double tanh(double x);

double exp(double x);
double frexp(double x, int* exponent);
double ldexp(double x, int n);
double log(double x);
double log2(double x);
double log10(double x);
double modf(double x, double* integer);

double pow(double x, double y);
double sqrt(double x);
double ceil(double x);
double fabs(double x);
double floor(double x);
double fmod(double x, double y);

//float versions
float acosf(float x);
float asinf(float x);
float atanf(float x);
float atan2f(float y, float x);
float cosf(float x);
float sinf(float x);
float tanf(float x);

float coshf(float x);
float sinhf(float x);
float tanhf(float x);

float expf(float x);
float frexpf(float x, int* exponent);
float ldexpf(float x, int n);
float logf(float x);
float log2f(float x);
float log10f(float x);
float modff(float x, float* integer);

float powf(float x, float y);
float sqrtf(float x);
float ceilf(float x);
float fabsf(float x);
float floorf(float x);
float fmodf(float x, float y);

#endif // MATH_H_