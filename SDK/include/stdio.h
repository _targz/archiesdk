#ifndef STDIO_H_
#define STDIO_H_
#include <stdarg.h>
#include <decls/FILE.h>
#include <decls/size_t.h>
#include <decls/NULL.h>

int puts(const char* string);
int putchar(int character);
int printf(const char* fmt, ...);
int sprintf(char* string, const char* fmt, ...);
int snprintf(char* string, size_t n, const char* fmt, ...);

int getchar(void);
char* gets(char* str);

void perror(const char* str);


//TBD
extern FILE* stdin;
extern FILE* stdout;
extern FILE* stderr;
#define stdin stdin
#define stdout stdout
#define stderr stderr
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

int fflush(FILE* stream);
int fprintf(FILE* stream, const char* fmt, ...);
int vfprintf(FILE* stream, const char* fmt, va_list arg);
int fclose(FILE* stream);
char* fgets(char* str, int n, FILE* stream);
int fputs(const char* str, FILE* stream);
FILE* fopen(const char* filename, const char* mode);
FILE* freopen(const char* filename, const char* mode, FILE* stream);
size_t fread(void* ptr, size_t size, size_t nmemb, FILE* stream);
int fseek(FILE* stream, long int offset, int whence);
long int ftell(FILE* stream);
size_t fwrite(const void* ptr, size_t size, size_t nmemb, FILE* stream);
int feof(FILE* stream);
int ferror(FILE* stream);
void clearerr(FILE* stream);
int getc(FILE* stream);
int ungetc(int ch, FILE* stream);
void setbuf(FILE* stream, char* buffer);
int setvbuf(FILE* stream, char* buffer, int mode, size_t size);
FILE* tmpfile(void);
char* tmpnam(char* str);
int remove(const char* pathname);
int rename(const char* old_filename, const char* new_filename);

#endif // STDIO_H_