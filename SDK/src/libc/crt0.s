.arch armv2a
.file	"crt0.s"
.section	.init
.align 2
.global	_start
.syntax unified
.arm
.fpu softvfp

_start:
	adrl sp, stack_end //setup stack

	mov r0, #0
	str r0, _atexit_funcs_p //Reset to 0

	//Setup heap
	mov r0, #-1
	mov r1, #-1
	swi 0x400ec //Wimp_SlotSize

	ldr r1, =_heapStart
	ldr r2, =_start
	subs r2, r1, r2
	subs r3, r0, r2
	mov r0, #0
	swi 0x1d //OS_Heap

	//Setup argc and argv
	bl _crt0_setupArgs

	//argc in r0, argv in r1. Ready for main
	bl main

	//Fall through to exit
.global exit
exit:
	push {r0} //Return code
	bl _atexit_call
	pop {r0}

	and r0, #0xFF //Ensure that we don't raise an error. We could alternatively set Sys$RCLimit to UINT32_MAX.
	mov r2, r0
	ldr r1, =0x58454241 //"ABEX"
	mov r0, 0
.global abort
abort:
	swi 0x11 //OS_Exit
.size _start, . - _start

.global _atexit_funcs_p
_atexit_funcs_p:
	.space 4

.align 3
stack_base:
	.space 1024
stack_end:
	.space 4
