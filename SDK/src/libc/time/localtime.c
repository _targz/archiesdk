#include <time.h>

struct tm* localtime(const time_t* timer){
    time_t tmp = *timer; /* todo, handle local timezone; should be just a matter of adding a constant? */
    /* if risc os < 3: use timer as is. else read territory info? */
    return gmtime(&tmp);
}
