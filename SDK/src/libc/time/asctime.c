#include <time.h>
#include <stdio.h>

#include "archie/SDKTypes.h"

static const char asctimeWdays[7][3] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
static const char asctimeWmonths[12][3] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
static char asctimeString[26];

char* asctime(const struct tm* timeptr){
    
    /* Invalid values are undefined behaviour. I choose to limit the values */

    u8 month = (timeptr->tm_mon > 11)? 11 : timeptr->tm_mon;
    u8 day = (timeptr->tm_wday > 6)? 6 : timeptr->tm_wday;
    u8 mday = (timeptr->tm_mday > 31)? 31 : timeptr->tm_mday;
    u8 hour = (timeptr->tm_hour > 23)? 23 : timeptr->tm_hour;
    u8 min = (timeptr->tm_min > 59)? 59 : timeptr->tm_min;
    u8 sec = (timeptr->tm_sec > 60)? 60 : timeptr->tm_sec;
    u16 year = timeptr->tm_year + 1900;
    if(year > 9999) year = 9999;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-overflow"
    /* GCC doesn't seem to calculate the region size correctly, leading to a few annoying warnings. I've spent half an hour trying to figure out 
       what causes this to no avail. So we tell GCC to shut up. I've quadruple checked my code, this is safe. */
    sprintf(asctimeString, "%.3s %.3s%3d %.2d:%.2d:%.2d %d\n", asctimeWdays[day], asctimeWmonths[month], mday, hour, min, sec, year);
#pragma GCC diagnostic pop

    /* manually set last char to \0 in case something went wrong */
    asctimeString[25] = '\0';

    return asctimeString;
}