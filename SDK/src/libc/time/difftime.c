#include <time.h>

double difftime(time_t time_end, time_t time_beg){
        return ((double)time_end - (double)time_beg)/(double)1000.0;
}