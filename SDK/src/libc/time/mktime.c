#include <time.h>
#include <stdbool.h>

#include "archie/SDKTypes.h"

bool __isleap(u32 yr) {
    return yr % 400 == 0 || (yr % 4 == 0 && yr % 100 != 0);
}

u32 __months_to_days(u32 month) {
    return (month * 3057 - 3007) / 100;
}

i32 __years_to_days(u32 yr) {
    return yr * 365L + yr / 4 - yr / 100 + yr / 400;
}

i32 __ymd_to_scalar(u32 yr, u32 mo, u32 day) {
    i32 scalar;

    scalar = day + __months_to_days(mo);
    if (mo > 2)                         /* adjust if past February */
        scalar -= __isleap(yr) ? 1 : 2;
    yr--;
    scalar += __years_to_days(yr);
    return (scalar);
}

time_t mktime(struct tm* timeptr){

    time_t tt;

    tt = __ymd_to_scalar(timeptr->tm_year + 1900, timeptr->tm_mon + 1, timeptr->tm_mday) - __ymd_to_scalar(1970, 1, 1);
    tt = tt * 24 + timeptr->tm_hour;
    tt = tt * 60 + timeptr->tm_min;
    tt = tt * 60 + timeptr->tm_sec;

    *timeptr = *gmtime(&tt);

    return tt;

}