#include <time.h>

#include "archie/SDKTypes.h"
#include "archie/SWI.h"

time_t time(time_t* timer){
    u64 curTime = 3;
    asm volatile("mov r0, #14\n" /* OSWord #14 doesn't have a name, so I won't bother with a define */
                 "mov r1, %0\n"
                 "swi " swiToConst(OS_Word) "\n"
                 :
                 : "r"(&curTime) /* swi expects a 5 byte array. A u64 is basically an 8 byte buffer. */
                 : "r0", "r1", "cc", "memory");
    curTime /= 100; /* RISC OS uses centiseconds, not seconds, so we convert */
    curTime -= 2208988800u; /* Convert to unix time */
    if(timer) *timer = curTime;
    return curTime;
}