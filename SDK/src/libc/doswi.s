.global _doswi

.align 4
_doswi:
	PUSH {r0-r11, lr}
	and r0, r0, #0x00FFFFFF //clean up MSB
	orr r0, r0, #0xEF000000 //convert to SWI 0xXXXXXX
	str r0, vswiJump //overwrite SWI

	mov r12, r1
	PUSH {r12}
	ldmia r12, {r0-r9}

vswiJump:
	SWI 0x11 //default to exit

	POP {r12}
	stmia r12, {r0-r9, r15}
	POP {r0-r11, pc}

