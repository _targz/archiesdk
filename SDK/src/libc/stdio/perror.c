#include <stdio.h>
#include <string.h>
#include <errno.h>

void perror(const char* str){
    if(str && *str) {
        puts(str);
        puts(": ");
    }
    puts(strerror(errno));
}