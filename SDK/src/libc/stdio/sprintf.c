#include <stdarg.h>
#include <string.h>

#include "archie/SDKTypes.h"

extern void do_printf(const char* fmt, va_list* argp);
extern int (*_pputchar)(int);
extern int (*_pputs)(const char*);
extern u32 _printf_nc;

static char* _stdio_curString;
static u32 _stdio_curStringPos;

int string_puts(const char* string) {
    u32 string_len = strlen(string);
    _printf_nc += string_len;
    memcpy(&(_stdio_curString[_stdio_curStringPos]), string, string_len);
    _stdio_curStringPos += string_len;
    return 1;
}

int string_putchar(int character) {
    ++_printf_nc;
    _stdio_curString[_stdio_curStringPos] = character;
    ++_stdio_curStringPos;
    return character;
}

int sprintf(char* string, const char* fmt, ...){
	va_list	listp;

    _pputchar = string_putchar;
    _pputs = string_puts;
    _stdio_curString = string;
    _stdio_curStringPos = 0;

	va_start(listp, fmt);
	do_printf(fmt, &listp);
	va_end(listp);
    string_putchar('\0');
    return _printf_nc-1;
}
