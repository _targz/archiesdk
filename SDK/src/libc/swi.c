#include <swis.h>
#include <archie/SDKTypes.h>

#include <stdio.h>

typedef struct {
	u32 r[16];
} registers;

extern void _doswi(int swi, registers* regs);

int _vswi(int swi, unsigned int mask, va_list* args){
    registers regs;
	u32 inputRegs = mask&0x3F;
	u32 outputRegs = (mask>>22)&0x3F;
    u32 i;

	for(i = 0; i < 11; ++i) regs.r[i] = (inputRegs & _IN(i))? va_arg(*args, u32) : 0;

	if(mask & (1<<11)) {
		u32 offset = 0;
		for(i = 21; i < 32; ++i) if(mask & (1<<i)) ++offset; /* Skip over output va args */
		regs.r[(mask&(0xF<<12))>>12] = (u32)&((*((u32**)args))[offset]); /* va_list is just a ptr to the extra variadic parameters */
	}
	
	_doswi(swi, &regs);
	
	for(i = 0; i < 11; ++i) {
		if(outputRegs & (1U << (31 - (i)))) *va_arg(*args, u32*) = regs.r[i];
	}

	if(mask&(1<<21)) *va_arg(*args, u32*) = regs.r[10];

	/* Returning reg > 9 is undefined */
	return (((mask>>16)&0xF) == 15)? regs.r[10] : regs.r[((mask>>16)&0xF)];
}

_kernel_oserror* _vswix(int swi, unsigned int mask, va_list* args){
    registers regs;
	u32 inputRegs = mask&0x3F;
	u32 outputRegs = (mask>>22)&0x3F;
    u32 i;

	for(i = 0; i < 11; ++i) regs.r[i] = (inputRegs & _IN(i))? va_arg(*args, u32) : 0;

	if(mask & (1<<11)) {
		u32 offset = 0;
		for(i = 21; i < 32; ++i) if(mask & (1<<i)) ++offset; /* Skip over output va args */
		regs.r[(mask&(0xF<<12))>>12] = (u32)&((*((u32**)args))[offset]); /* va_list is just a ptr to the extra variadic parameters */
	}
	
	_doswi(swi | XOS_Bit, &regs);
	
	for(i = 0; i < 11; ++i) {
		if(outputRegs & (1U << (31 - (i)))) *va_arg(*args, u32*) = regs.r[i];
	}

	if(mask&(1<<21)) *va_arg(*args, u32*) = regs.r[10];

	return (regs.r[10]&(1<<28))? (_kernel_oserror*)regs.r[0] : (_kernel_oserror*)NULL; /* Return r0 if overflowing */
}

int _swi(int swi, unsigned int mask, ...){
    int retVal;
	va_list	listp;
	va_start(listp, mask);
	retVal = _vswi(swi, mask, &listp);
	va_end(listp);
    return retVal;
}

_kernel_oserror* _swix(int swi, unsigned int mask, ...){
    _kernel_oserror* retVal;
	va_list	listp;
	va_start(listp, mask);
	retVal = _vswix(swi, mask, &listp);
	va_end(listp);
    return retVal;
}
