int isblank(int character){
    return (character == ' ') || (character == '\t');
}