#include <ctype.h>

int isalpha(int character){
    return islower(character) || isupper(character);
}