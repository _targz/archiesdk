#include <ctype.h>

int isxdigit(int character){
    int upperchar = toupper(character);
    return isdigit(character) || ((upperchar >= 'A') && (upperchar <= 'F'));
}