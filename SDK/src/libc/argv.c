#include <string.h>
#include <stdlib.h>
#include <swis.h>
#include <archie/SWI.h>
#include <archie/SDKTypes.h>

u64 _crt0_setupArgs(void) {
    u32 argc = 1; /* Used for return value */
    u8** argv;

    char* cmdline; /* Pointer to original cmdline */
    u8* argvString; /* Pointer to copied and modified cmdline */
    u8* tmpPtr; /* Temp pointer used for copying and reading */
    u32 i;
    
    /* Grab cmdline */
    asm volatile("  SWI " swiToConst(OS_GetEnv) "\n\
                    mov %0, r0" : "=r"(cmdline) : : "r0", "r1", "r2");

    /* Copy cmdline, swap spaces for NULL terminators, get argc */
    argvString = tmpPtr = malloc(strlen(cmdline)+1);
    while(*cmdline) {
        if(*cmdline == ' ') {
            ++argc;
            *tmpPtr = '\0';
        } else *tmpPtr = *cmdline;
        ++cmdline;
        ++tmpPtr;
    }
    *tmpPtr = '\0';
    
    /* Alloc argv */
    argv = malloc(sizeof(u8*)*(argc+1));

    /* Prepare argv limits */
    argv[0] = argvString;
    argv[argc] = (u8*)NULL;

    /* Fill out argv */
    tmpPtr = argvString;
    for(i = 1; i < argc; ++i) {
        while(*tmpPtr++);
        argv[i] = tmpPtr;
    }

    /* argc in r0, argv in r1. Ready for main() */
    return (u64)argc | (((u64)((u32)argv))<<32); 
}