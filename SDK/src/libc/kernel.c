#include <swis.h>
#include <kernel.h>
#include <string.h>
#include <archie/SDKTypes.h>

typedef struct {
	u32 r[16];
} registers;

extern void _doswi(int swi, registers* regs);

_kernel_oserror* _kernel_swi_c(int no, _kernel_swi_regs* in, _kernel_swi_regs* out, int* carry){
    registers regs;
    memcpy(&regs, in, sizeof(_kernel_swi_regs));
    _doswi((no & _kernel_NONX)? (no & ~_kernel_NONX) : ((no & ~_kernel_NONX) | XOS_Bit), &regs);
    memcpy(out, &regs, sizeof(_kernel_swi_regs));
    if(carry) *carry = regs.r[10]&_C;
    return (regs.r[10]&(1<<28))? (_kernel_oserror*)regs.r[0] : (_kernel_oserror*)NULL; /* Return r0 if overflowing */
}

_kernel_oserror* _kernel_swi(int no, _kernel_swi_regs* in, _kernel_swi_regs* out){
    return _kernel_swi_c(no, in, out, NULL);
}