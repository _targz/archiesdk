#include <stdlib.h>
#include <string.h>

void* calloc(size_t nitems, size_t size){
    const size_t totalSize = size*nitems;
    void *ptr = malloc(totalSize);
    if (!ptr)
        return NULL;
    memset(ptr, 0, totalSize);
    return ptr;
}
