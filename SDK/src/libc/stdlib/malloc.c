#include <stdlib.h>

#include "archie/SDKTypes.h"
#include "archie/SWI.h"

extern u32 _heapStart;
void* malloc(size_t size){

    u32* returnVal = NULL;
    asm volatile("mov r0, #2\n"
                 "ldr r1, =_heapStart\n"
                 "mov r3, %1\n"
                 "swi " swiToConst(OS_Heap | XOS_Bit)"\n"
                 "mov %0, r2\n"
                 : "=r"(returnVal)
                 : "r"(size+4)
                 : "r0", "r1", "r2", "r3", "cc", "memory");
    if (!returnVal)
        return NULL;
    *returnVal = size+4;
    return returnVal+1;

}
