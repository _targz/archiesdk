#include "archie/SDKTypes.h"

static i32 __rand_seed = 0;

int rand(void){
    return __rand_seed = 1103515245 * __rand_seed + 12345;
}

void srand(unsigned int seed){
    __rand_seed = seed;
}
