#include <stdio.h>
#include <string.h>
#include <archie/SWI.h>
#include <swis.h>

static char _envVar[257];

char* getenv(const char* name){

    char* tmpPtr = &_envVar[0];

    memset(&_envVar[0], 0, 257); /* Zero out envVar */
    asm volatile("  mov r0, %1\n\
                    mov r1, %0\n\
                    mov r2, #-1\n\
                    mov r3, #0\n\
                    mov r4, #0\n /* Leave expansion to the user */ \
                    SWI " swiToConst((OS_ReadVarVal | XOS_Bit)) "\n\
                    \
                    cmp r2, #0\n\
                    moveq %0, #0\n /* Return NULL if 0 bytes were read */\
                    beq %=f \n\
                    \
                    mov r0, %1\n\
                    mov r1, %0\n\
                    mov r2, #256\n\
                    mov r3, #0\n\
                    mov r4, #0\n /* Leave expansion to the user */ \
                    SWI " swiToConst(OS_ReadVarVal | XOS_Bit) "\n\
                    %=:" : "+r"(tmpPtr) : "r"(name) : "r0", "r1", "r2", "r3", "r4");

    return tmpPtr;
}