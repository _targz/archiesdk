#include <stdlib.h>
#include <archie/SWI.h>
#include <archie/SDKTypes.h>
#include <swis.h>

int system(const char* string) {
    if(!string) return 1;
    asm volatile("  mov r0, %0\n\
                    SWI " swiToConst(OS_CLI) : "=r"(string) : : "r0");
    
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
    return *((u32*)getenv("Sys$ReturnCode"));
#pragma GCC diagnostic pop
}