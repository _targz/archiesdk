#include "archie/SDKTypes.h"
#include "archie/SWI.h"

void free(void* ptr){
    if(!ptr) return;
    asm volatile("mov r0, #3\n"
                 "ldr r1, =_heapStart\n"
                 "mov r2, %0\n"
                 "swi " swiToConst(OS_Heap | XOS_Bit)"\n"
                 :
                 : "r"(((u8*)ptr)-4)
                 : "r0", "r1", "r2", "cc", "memory");
}
