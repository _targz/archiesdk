#include <stdlib.h>

div_t div(int numer, int denom){
    div_t tmp;
    tmp.quot = (numer/denom);
    tmp.rem = (numer%denom);
    return tmp;
}