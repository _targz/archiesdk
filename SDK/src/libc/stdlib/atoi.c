#include <stdbool.h>
#include <ctype.h>

#include "archie/SDKTypes.h"

bool _stdlib_isWhitespace(const char character){
    return character == 32;
}

int atoi(const char *str){
    i32 tmp = 0;
    
    /* nom whitespace :3 */
    while(_stdlib_isWhitespace(*str)) ++str;

    if(*str == '-') {
        tmp = -0;
        ++str;
    }
    while(isdigit(*str)) {
        tmp = (tmp*10) + (*str - 48);
        ++str;
    }
    return tmp;
}