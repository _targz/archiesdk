#include <locale.h>
#include <limits.h>

static struct lconv _localeconvBuf;

char* setlocale(int category, const char* locale){
    (void)category;
    (void)locale;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
    return "C";
#pragma GCC diagnostic pop
}

struct lconv* localeconv(void){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
    _localeconvBuf.decimal_point = ".";
    _localeconvBuf.thousands_sep = "";
    _localeconvBuf.grouping = "";
    _localeconvBuf.mon_decimal_point = "";
    _localeconvBuf.mon_thousands_sep = "";
    _localeconvBuf.mon_grouping = "";
    _localeconvBuf.positive_sign = "";
    _localeconvBuf.negative_sign = "";
    _localeconvBuf.currency_symbol = "";
    _localeconvBuf.frac_digits = CHAR_MAX;
    _localeconvBuf.p_cs_precedes = CHAR_MAX;
    _localeconvBuf.n_cs_precedes = CHAR_MAX;
    _localeconvBuf.p_sep_by_space = CHAR_MAX;
    _localeconvBuf.n_sep_by_space = CHAR_MAX;
    _localeconvBuf.p_sign_posn = CHAR_MAX;
    _localeconvBuf.n_sign_posn = CHAR_MAX;
    _localeconvBuf.int_curr_symbol = "";
    _localeconvBuf.int_frac_digits = CHAR_MAX;
    _localeconvBuf.int_p_cs_precedes = CHAR_MAX;
    _localeconvBuf.int_n_cs_precedes = CHAR_MAX;
    _localeconvBuf.int_p_sep_by_space = CHAR_MAX;
    _localeconvBuf.int_n_sep_by_space = CHAR_MAX;
    _localeconvBuf.int_p_sign_posn = CHAR_MAX;
    _localeconvBuf.int_n_sign_posn = CHAR_MAX;
#pragma GCC diagnostic pop
    return &_localeconvBuf;
}