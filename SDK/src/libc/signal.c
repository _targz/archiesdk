#include <signal.h>

__sighandler_t signal(int sig, __sighandler_t func){
    (void)sig;
    (void)func;
    return SIG_DFL;
}

int kill(__pid_t pid, int sig){
    (void)sig;
    if(pid == -1) return pid;
    return 0;
}

int raise(int sig){
    (void)sig;
    return 0;
}