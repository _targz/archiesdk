#include <string.h>

char* strpbrk(const char* str1, const char* str2){
    size_t str2len = strlen(str2);
    
    while(*str1){
        size_t i;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
        for(i = 0; i < str2len; ++i) if(*str1 == str2[i]) return str1; /* once again following the standard here */
#pragma GCC diagnostic pop
        ++str1;
    }

    return NULL;
}