char* strcat(char* dest, const char* src){
    char* tmp = dest;
    while (*dest) dest++; /* skip to end */
    while ((*dest++ = *src++)); /* copy */
    return tmp;
}