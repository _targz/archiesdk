#include <string.h>
#include <archie/SDKTypes.h>

void* memchr(const void* str, int c, size_t n){
    u8 character = (u8)c;
    const u8* string = (const u8*)str;
    while(n--) {
        
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
        if(*string == character) return (void*)string; /* Blame the standard */
#pragma GCC diagnostic pop

        ++string;
    }
    return NULL;
}