#include <string.h>

char* strncat(char* dest, const char* src, size_t n){
    char* tmp = dest;
    size_t srcSize = strnlen(src, n);

    dest += strlen(dest);

    memcpy(dest, src, srcSize);
    dest[srcSize] = '\0';

    return tmp;
}