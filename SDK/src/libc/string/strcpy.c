char* strcpy(char* dest, const char* src){
    char* tmp = dest;
    while((*dest++ = *src++));
    return tmp;
}
