static const char* _strerrorString = "Error, probably"; /* Placeholder */

char* strerror(int errnum){
    (void)errnum;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
    return _strerrorString;
#pragma GCC diagnostic pop
}