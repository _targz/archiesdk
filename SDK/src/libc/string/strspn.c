#include <string.h>
#include <stdbool.h>

#include "archie/SDKTypes.h"

size_t strspn(const char* str1, const char* str2){
    size_t nChars = 0;
    while(1) {
        u32 i;
        size_t str2len = strlen(str2);
        bool found = false;
        for(i = 0; i < str2len; ++i){
            if(*str1 == str2[i]) found = true;
        }
        if(!found) return nChars;
        ++str1;
        ++nChars;
    }
}
