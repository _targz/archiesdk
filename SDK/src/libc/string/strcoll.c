#include "archie/SDKTypes.h"

int strcoll(const char* str1, const char* str2){
	while (*str1 == *str2++) if (!*str1++) return 0; 
	return *(const u8*)str1 - *(const u8*)(--str2);
}