#include <string.h>

size_t strnlen(const char* string, size_t n){
    size_t size = 0;
    while(*string && size < n) {
        ++size;
        ++string;
    }
    return size;
}
