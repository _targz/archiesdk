#include <string.h>

#include "archie/SDKTypes.h"

void* memset(void* dest, register const int c, size_t size){
    register u8* destCpy = (u8*)dest;
    while(size--){
        *destCpy = c;
        ++destCpy;
    }
    return dest;
}
