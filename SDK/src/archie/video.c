#include "archie/video.h"
#include "archie/SWI.h"

/* make sure your colour is 0x00RRGGBB */
void v_setBorderColour(u32 colour){
    u8 buffer[8];
    buffer[0] = buffer[1] = 24;
    buffer[2] = (colour>>16)&255;
    buffer[3] = (colour>>8)&255;
    buffer[4] = colour&255;
    asm volatile("mov r0, #12\n"
                 "mov r1, %0\n"
                 "swi " swiToConst(OS_Word) : : "r"(buffer): "r0", "r1", "cc", "memory");
}

void v_setBorderColourRGB(u8 r, u8 g, u8 b){
    v_setBorderColour(r<<16 | g<<8 | b);
}

void v_waitForVSync(void){
    asm volatile("mov r0, " swiToConst(OSByte_Vsync) "\n"
                 "swi " swiToConst(OS_Byte) : : : "r0", "r1", "r2", "cc");
}

void v_setMode(u32 mode){
    asm volatile("mov r0, #22\n"
                 "swi "swiToConst(OS_WriteC)"\n"
                 "mov r0, %0\n"
                 "swi " swiToConst(OS_WriteC) : : "r" (mode) : "r0", "cc");
}

void v_enableVSync(void){
    asm volatile("mov r0, " swiToConst(OSByte_EventEnable) "\n"
                 "mov r1, #4\n"
                 "swi "swiToConst(OS_Byte) : : : "r0", "r1", "r2", "cc");
}

void v_disableVSync(void){
    asm volatile("mov r0, " swiToConst(OSByte_EventDisable) "\n"
                 "mov r1, #4\n"
                 "swi "swiToConst(OS_Byte) : : : "r0", "r1", "r2", "cc");
}

void* v_getScreenAddress(void){
    void* screen_addr;
    const i32 screen_addr_input[2] = {VD_ScreenStart , -1};
    asm volatile("mov r0, %0\n"
                 "mov r1, %1\n"
                 "swi "swiToConst(OS_ReadVduVariables) : : "r" (&(screen_addr_input[0])), "r" (&screen_addr): "r0", "r1", "cc", "memory");

    return screen_addr;

}

void v_disableTextCursor(void){
    asm volatile("mov r0, #23\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "mov r0, #1\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "mov r0, #0\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "swi " swiToConst(OS_WriteC) "\n"
                 "swi " swiToConst(OS_WriteC) : : : "r0", "cc");
}
