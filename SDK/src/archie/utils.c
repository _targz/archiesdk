#include "archie/utils.h"
#include "archie/SWI.h"

void p_clearConsole(void){
    asm volatile("swi " swiToConst(OS_WriteI) "+12" : : : "cc", "memory");
}