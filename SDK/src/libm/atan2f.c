#include <math.h>

#include "archie/SDKTypes.h"

typedef union {
  u32 u;
  i32 i;
  float f;
} u_isf;

float atan2f(float y, float x){
    u_isf yfi, xfi, atan_1q, uatan_2q;
    u32 ux_s, uy_s;
    float q, bxy_a, num;
    static const float b = 0.596227f;
    yfi.f = y;
    xfi.f = x;

    /* Extract the sign bits */
    ux_s = (u32)0x80000000 & xfi.u;
    uy_s = (u32)0x80000000 & yfi.u;

    /* Determine the quadrant offset */
    q = (float)( ( ~ux_s & uy_s ) >> 29 | ux_s >> 30 ); 

    /* Calculate the arctangent in the first quadrant */
    bxy_a = fabsf( b * xfi.f * yfi.f );
    num = bxy_a + yfi.f * yfi.f;
    atan_1q.f =  num / ( xfi.f * xfi.f + bxy_a + num );

    /* Translate it to the proper quadrant */
    uatan_2q.u = (ux_s ^ uy_s) | atan_1q.u;

    return q + uatan_2q.f;
}