#include <math.h>

float atanf(float x){
    if(fabsf(x) > 1233.952637f) return(signbit(x)? -1.569986f : 1.569986f);
    return M_PI_4*x - x*(fabsf(x) - 1)*(0.2447f + 0.0663f*fabsf(x));
}