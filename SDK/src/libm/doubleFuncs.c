#include <math.h>

inline double acos(double x) { return (double)acosf((float)x); }
inline double asin(double x) { return (double)asinf((float)x); }
inline double atan(double x) { return (double)atanf((float)x); }
inline double atan2(double y, double x) { return (double)atan2f((float)y, (float)x); }
inline double cos(double x) { return (double)cosf((float)x); }
inline double sin(double x) { return (double)sinf((float)x); }
inline double tan(double x) { return (double)tanf((float)x); }
inline double cosh(double x) { return (double)coshf((float)x); }
inline double sinh(double x) { return (double)sinhf((float)x); }
inline double tanh(double x) { return (double)tanhf((float)x); }
inline double exp(double x) { return (double)expf((float)x); }
inline double frexp(double x, int* exponent) { return (double)frexpf((float)x, exponent); }
inline double ldexp(double x, int n) { return (double)ldexpf((float)x, n); }
inline double log(double x) { return (double)logf((float)x); }
inline double log10(double x) { return (double)log10f((float)x); }
inline double pow(double x, double y) { return (double)powf((float)x, (float)y); }
inline double sqrt(double x) { return (double)sqrtf((float)x); }
inline double ceil(double x) { return (double)ceilf((float)x); }
inline double fabs(double x) { return (double)fabsf((float)x); }
inline double floor(double x) { return (double)floorf((float)x); }
inline double fmod(double x, double y) { return (double)fmodf((float)x, (float)y); }

inline double modf(double x, double* integer) { 
    float tmpFloat = (float)*integer;
    float ret = modff((float)x, &tmpFloat);
    *integer = (double)tmpFloat;
    return (double)ret;
}