#include <math.h>

float sinhf(float x){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(x == -0.0f) return x;
#pragma GCC diagnostic pop
    if(fabsf(x) > 89.41f) return(signbit(x)? -INFINITY : INFINITY); /* hacky */
    return 0.5f * (expf(x) - 1.0f/expf(x));
}