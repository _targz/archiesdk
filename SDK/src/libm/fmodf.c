#include <math.h>

float fmodf(float x, float y){
    float divRes = fabsf(x / y);
    divRes = (divRes - floorf(divRes)) * y;
    return (signbit(x) ^ signbit(y)) ? -divRes : divRes;
}