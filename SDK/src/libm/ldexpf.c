
#include "archie/SDKTypes.h"

typedef union {
  u32 u;
  i32 i;
  float f;
} u_isf;

float ldexpf(float x, int n){
	u_isf tmpInt;

	if(n > 127){
		n -= 127;
		x *= 1.7014118E38f;
		if(n > 127){
			n -= 127;
			x *= 1.7014118E38f;
			if(n > 127) n = 127;
		}
	} else if(n < -126){
		n += 102;
		x *= 1.9721523E-31f;
		if (n < -126) {
			n += 102;
			x *= 1.9721523E-31f;
			if(n < -126) n = -126;
		}
	}

    tmpInt.i = (u32)(n+127)<<23;
	return x * tmpInt.f;
}