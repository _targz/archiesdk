#include <math.h>

#include "archie/SDKTypes.h"

float powf(float x, float y){
    float tmp = expf(y * logf(fabsf(x)));
    if(x < 0.0f){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
        if((i32)y == y) return fmodf(y, 2)? -tmp : tmp;
        else return -NAN;
#pragma GCC diagnostic pop
    }
    return tmp;
}