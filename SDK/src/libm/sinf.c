#include <math.h>

float sinf(float x){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(x == 0.0f) return x;
#pragma GCC diagnostic pop
    return cosf(x-M_PI_2);
}