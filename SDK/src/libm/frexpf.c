#include <math.h>

#include "archie/SDKTypes.h"

float frexpf(float x, int* exponent){
    i32* xInt = (i32*)&x;
	i32 xExponent = (*xInt>>23)&0xff;

    if(xExponent == 0xff) return x; /* x is nan or infinity, return x */

	if(!xExponent){ /* x is 0 or subnormal */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
		if(!x) *exponent = 0; /* x is 0, set exp to 0 and return x */
        else {
			x = frexpf(x*0x1p64f, exponent);
			*exponent -= 64;
		}
#pragma GCC diagnostic pop
		return x;
	}

	*exponent = xExponent-126; /* write exponent */
    *xInt = (*xInt&0x807fffff)|0x3f000000; /* write fraction */
	return x;
}