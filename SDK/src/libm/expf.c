#include <math.h>

#include "archie/SDKTypes.h"

typedef union {
    u32 u;
    i32 i;
    float f;
} u_isf;

float expf(float x){
    float intPart, deciPart;
    u_isf exponent;

    if(x > 89.41f) return INFINITY; /* hacky */
    if(x < -103.963799f) return 0.0f;

    x *= M_LOG2E;

    intPart = floorf(x + 0.5f);
    deciPart = x - intPart;
    exponent.i = (((i32)intPart)+127)<<23;

    return (fabsf(x)<0.5001f? 0.0f : (signbit(x)? -0.5f : 1.0f)) + (exponent.f*(((((0.00015353362f*deciPart + 0.0013398874f)*deciPart + 0.009618437f)*deciPart + 0.055503324f)*deciPart + 0.24022648f)*deciPart + 0.6931472f)*deciPart + 1.0f);

}