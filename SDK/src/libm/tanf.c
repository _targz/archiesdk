#include <math.h>

float tanf(float x) {
    return sinf(x)/cosf(x);
}