#include <math.h>

#include "archie/SDKTypes.h"

float floorf(float x) {
    u32 xInt;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(!isfinite(x) || (x == -0)) return x;

    xInt = (u32)fabsf(x);
    if(xInt == fabsf(x)) return x;
#pragma GCC diagnostic pop

    if(x>0) return (float)xInt;
    else return 0.0f-((float)(xInt+1));
}