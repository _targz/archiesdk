#!/bin/bash
set -e

echo "Cleaning up build folders (if they exist)"
rm -rf tools/binutils-build tools/gcc-build

echo Creating build folders...
mkdir -p tools/binutils-build
mkdir -p tools/gcc-build

echo Preparing variables...
export TOOLSPATH=$(realpath tools/)
export SYSROOT=$(realpath SDK/)
export ARCHIESDK=$(realpath .)
NCPU=$(nproc)

echo Downloading and unpacking gcc and binutils...
cd $TOOLSPATH
if [ ! -f gcc.tar.gz ]; then
    rm -f gcc.tar.gz.tmp
    wget -O gcc.tar.gz.tmp https://gitlab.com/_targz/gcc/-/archive/master/gcc-master.tar.gz
    mv gcc.tar.gz.tmp gcc.tar.gz
    tar xvf gcc.tar.gz
fi
if [ ! -f binutils.tar.gz ]; then
    rm -f binutils.tar.gz.tmp
    wget -O binutils.tar.gz.tmp https://gitlab.com/_targz/binutils/-/archive/master/binutils-master.tar.gz
    mv binutils.tar.gz.tmp binutils.tar.gz
    tar xvf binutils.tar.gz
fi

echo Downloading GCC dependencies...
cd gcc-master/
./contrib/download_prerequisites

echo Making binutils...
cd ../binutils-build/
../binutils-master/configure --prefix=$TOOLSPATH --target=arm-archie --with-sysroot=$SYSROOT --with-build-sysroot=$SYSROOT --disable-nls --disable-multilib
make -j$NCPU configure-host
make -j$NCPU
make install

echo Making GCC...
cd ../gcc-build/
../gcc-master/configure --prefix=$TOOLSPATH --target=arm-archie --with-sysroot=$SYSROOT --with-build-sysroot=$SYSROOT --disable-nls --disable-shared --enable-languages=c --disable-multilib --disable-threads --disable-decimal-float --disable-libgomp --disable-libmudflap --disable-libssp --disable-libatomic --disable-libquadmath -with-cpu=arm2 --with-float=soft
make -j$NCPU all-gcc all-target-libgcc CFLAGS_FOR_TARGET='-msoft-float -mcpu=arm2 -mno-thumb-interwork -O2 -ffreestanding'
make -j$NCPU install-gcc install-target-libgcc

echo Making SDK...
cd $SYSROOT/
make sdk

echo Building Info-ZIP..
cd $ARCHIESDK
./build-infozip.sh

echo Making examples...
cd $ARCHIESDK/examples
make

# Add an empty line before and after final message to make it more visible
echo 
echo All done! Please remember to add \'export ARCHIESDK=\"$ARCHIESDK\"\' to your environment! \(.bashrc\; /etc/environment or /etc/profile\)
echo 
