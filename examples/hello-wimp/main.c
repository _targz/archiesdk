#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <swis.h>
#include <kernel.h>

#include "archie/video.h"
#include "archie/keyboard.h"

u32 task_handle;
u8 wimp_block[256];

int main(void){
    bool quit = false;
    
    _kernel_swi_regs regs;
    regs.r[0] = 300;
    regs.r[1] = *(int *)"TASK";
    regs.r[2] = (int)"Hello WIMP!";
    _kernel_swi(Wimp_Initialise, &regs, &regs);
    task_handle = regs.r[1];
    
    // task_handle = _swi(Wimp_Initialise, _INR(0,2)|_RETURN(1), 300, *(int *)"TASK", "Hello WIMP!");
    
    _swi(Wimp_CreateIcon, _IN(0)|_BLOCK(1)|_RETURN(0), 0, -1, 0, 0, 68, 68, 2 | (3<<12), *(int *)"appl", *(int *)"icat", *(int *)"ion");
    
    while (!quit) {
        int pollword;
        int reason = _swi(Wimp_Poll, _INR(0,2)|_RETURN(0), 0x3831, wimp_block, &pollword);

        switch (reason) {
            case 0x11:
            case 0x12:
                if (wimp_block[16] == 0x0u) quit = true;
                break;
            default:
                break;
        }

    }
    
    _swi(Wimp_CloseDown, _INR(0,1), task_handle, *(int *)"TASK");
	
    return 0;
}
