#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include "archie/video.h"
#include "archie/keyboard.h"

//This example doesn't do anything useful, it just shows how to mix C and ASM
//We show how to write ASM functions that take no arguments, one argument
//more than one argument and how to return a value.
//We also show how to call C functions from ASM with no arguments, one argument
//more than one argument and how to handle return values.
//And lastly we show how to use inline assembly inside of C functions.

//This example only covers the bare minimum, we don't cover functions with tons
//of arguments and the inline asm example is very basic; but it should be 
//enough to get you started. I highly recommend you go look at GCC's "Extended
//ASM" page for more information.


//ASM function signatures

u32 asmPrintConst(void); 
//prints a string constant defined in asm.s and returns 0xBABABABA

void asmPrintOneArg(const char* text); 
//prints a string constant passed as argument and returns nothing

void asmPrintTwoArgs(const char* text1, const char* text2); 
//prints two string constants and returns nothing 

void asmPrintReturnValue(void);
//calls returnConstString and prints whatever that returned

void asmCallCFunctions(void);
//calls the C Functions declared below


//C Functions

void cPrintOneArg(const char* text){ //called by asmCallCFunctions
    puts(text);
}

void cPrintTwoArgs(const char* text1, const char* text2){ //called by asmCallCFunctions
    puts(text1);
    puts(text2);
}

const char* returnConstString(void) { //called by asmPrintReturnValue
    return "returnConstString";
}

//inline asm example
const char* inlineAsmExample(const char* string){
    const char* val;

    //Do not, under any circumstances, try to call a c function from inline asm
    //You will need to generate a clobber list for that and it is not worth the
    //effort at all. If you absolutely need to call a function then use 
    //multiple inline asm declarations and backup the state via input/output 
    //parameters or just put your routine in an asm file. Inline asm should
    //only be used for very short functions that use few registers or variables

    asm("mov r0, %1\n" //put our second asm parameter into r0. in this case our
                       //second asm parameter is the input variable string.
                       //At compile time, GCC will load string into a register
                       //and "%1" will be replaced with said register

        "add r0, r0, #9\n" //add 9 to string (to skip over "skipthis ")

        "mov %0, r0" //put r0 into the first asm parameter. In this case that
                     //is the output variable val. "%0" will be replaced by a
                     //register selected by GCC at compile time, and GCC will
                     //then put the value from said register into val

    : "=r"(val) //outputs
    : "r"(string) //inputs
    : "r0", "cc"); //clobbered registers
    //Clobbers tell GCC which registers have been modified. CC is the flags
    //register. If your inline asm block modifies RAM, make sure to add
    //"memory" to the clobbers. 
    //Clobbers are very important. Always tell GCC about your clobbers. 

    return val;
}

void quit(){
    v_disableVSync();
    //flush last vsync
    v_waitForVSync();
}

void init(){ 
    v_setMode(13);
    v_disableTextCursor();
    v_enableVSync();

    atexit(quit); //register exit callback
}

int main(){
    
    init();
    //call ASM functions
    printf("asmPrintConst returned %#lX\r\n", asmPrintConst()); 
    asmPrintOneArg("asmPrintOneArg"); 
    asmPrintTwoArgs("asmPrintTwoArgs1", "asmPrintTwoArgs2"); 
    asmPrintReturnValue();
    asmCallCFunctions();

    puts(inlineAsmExample("skipthis inlineAsmExample"));
    //this will print "inlineAsmExample" as inlineAsmExample skips over 
    //"skipthis "

	return 0;
}