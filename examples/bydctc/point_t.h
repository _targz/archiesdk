#ifndef POINT_T_H_
#define POINT_T_H_

#include "archie/SDKTypes.h"

typedef struct {
    i32 z;
    i32 x;
    i32 y;
} point_t;

#endif // POINT_T_H_