#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h> //for isfinite(). make sure to link with libm (-lm)
#include <libgen.h>

typedef struct {
    float x, y, z;
} point_t;

//used to prescale y
#define yScale 320

//scales and converts an obj into a randomised points array. 

int main(int argc, char* argv[]){
    
    FILE* obj;
    unsigned int vertices = 0;
    float objScale;
    char line[1024];
    unsigned char colour = 255; //default to white

    if((argc < 3) || (argc > 4)) {
        printf("Usage: %s [path to obj file] [object scale] ([object colour]) > output.h\nObject colour is a mode 13 colour value. It is optional and the default value is white (255)\n", argv[0]);
        return -1;
    }

    obj = fopen(argv[1], "rb");
    if(!obj) {
        printf("Could not open %s! Exiting...\n", argv[1]);
        return -1;
    }

    objScale = atof(argv[2]);
    if(!isfinite(objScale)) {
        printf("Invalid scale %s! Exiting...\n", argv[2]);
        return -1;
    }
    
    if(argc == 4) colour = (unsigned char)atoi(argv[3]);

    while(1){ 
        int res = fscanf(obj, "%s", line);
        if(res == EOF) break;
        if(!strcmp(line, "v")){ //vertex
            ++vertices;
        }
    }
    
    point_t* points = malloc(sizeof(point_t)*vertices);
    printf("#include \"archie/SDKTypes.h\"\n#include \"point_t.h\"\n#define %.*sVerticesLen %u\n#define %.*sColour %u\n\npoint_t %.*sVertices[%u] = {", (int)strlen(basename(argv[1])) - 4, basename(argv[1]), vertices, (int)strlen(basename(argv[1])) - 4, basename(argv[1]), colour, (int)strlen(basename(argv[1])) - 4, basename(argv[1]), vertices);
    
    vertices = 0;
    rewind(obj);

    while(1){ 
        int res = fscanf(obj, "%s", line);
        if(res == EOF) break;
        if(!strcmp(line, "v")){ //vertex
            fscanf(obj, "%f %f %f\n", &points[vertices].x, &points[vertices].y, &points[vertices].z);
            ++vertices;
        }
    }

    if (vertices > 1) {
        for (size_t i = 0; i < vertices - 1; ++i) {
            point_t tmpPoint;
            size_t j = i + rand() / (RAND_MAX / (vertices - i) + 1);

            tmpPoint.x = points[j].x;
            tmpPoint.y = points[j].y;
            tmpPoint.z = points[j].z;

            points[j].x = points[i].x;
            points[j].y = points[i].y;
            points[j].z = points[i].z;

            points[i].x = tmpPoint.x;
            points[i].y = tmpPoint.y;
            points[i].z = tmpPoint.z;
        }
    }

    for(int i = 0; i < vertices; ++i) {
        printf("{%d,%d,%d},", (int32_t)(points[i].z * objScale), (int32_t)(points[i].x * objScale), (int32_t)(points[i].y * objScale) * yScale);
    }

    printf("};");

    free(points);

    fclose(obj);

    return 0;
}