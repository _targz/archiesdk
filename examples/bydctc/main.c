/* 

    "Bet you didn't C that coming" 
    A little demo written by Targz to show off/test ArchieSDK.
    More info in the scroller so build this and have a look :)

    Code by Targz, GFX by Targz and Tôbach

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <math.h>
#include <archie/video.h>
#include <archie/keyboard.h>
#include <archie/SWI.h>

//data
#include "data/models/acorn.h"
#include "data/models/cModel.h"
#include "data/models/owl.h"
#include "data/gfx/fontData.h"
// #include "data/gfx/moireGfx.h"
#include "data/gfx/slpbuggy.h"
#include "data/gfx/ball.h"
#include "data/gfx/ball2.h"
#include "data/gfx/ball3.h"
#include "data/gfx/ball4.h"

//QTM SWIs
#define QTM_Load 0x47e40
#define QTM_SetSampleSpeed 0x47e49
#define QTM_Start 0x47e41
#define QTM_Clear 0x47e44

// helper macros
#define ALIGN_PTR_DOWN(ptr) (void *)((intptr_t)(ptr) & ~3)

//types
typedef struct {
    bool enabled;
    i32 x, y, z;
    i32 velX, velY, velZ;
} bouncyBall;

//constants
#define nBalls 28
const unsigned char* ballGfxArray[5] = {
    ballGfx,
    ballGfx2,
    ballGfx3,
    ballGfx4
};
const u32 ballShadowPalette[] = {0, 0, 0x01010101, 0x01010101, 0x01010101, 0x02020202, 0x02020202, 0x02020202, 0x28282828, 0x28282828, 0x28282828, 0x28282828, 0x28282828, 0x28282828, 0x28282828, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x29292929, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0x2a2a2a2a, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0, 0xc0c0c0c0};
const u32 moireFadePalette[] = {0,1,2,3,44,45,46,47,252,253,254,255};
const u32 moireSineWithDither[] = {0xe0e0e0e,0xe0e0e0e,0xe0d0e0d,0xe0d0e0d,0xe0d0e0d,0xd0d0d0d,0xd0d0d0d,0xd0d0d0d,0xd0c0d0c,0xd0c0d0c,0xd0c0d0c,0xc0c0c0c,0xc0c0c0c,0xc0c0c0c,0xc030c03,0xc030c03,0x3030303,0x3030303,0x3030303,0x3020302,0x3020302,0x3020302,0x2020202,0x2020202,0x2020202,0x2010201,0x2010201,0x2010201,0x1010101,0x1010101,0x1010101,0x1010101,0x1000100,0x1000100,0x1000100,0x1000100,0x1000100,0x1000100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x1000100,0x1000100,0x1000100,0x1000100,0x1000100,0x1000100,0x1010101,0x1010101,0x1010101,0x1010101,0x2010201,0x2010201,0x2010201,0x2020202,0x2020202,0x2020202,0x3020302,0x3020302,0x3020302,0x3030303,0x3030303,0x3030303,0xc030c03,0xc030c03,0xc0c0c0c,0xc0c0c0c,0xc0c0c0c,0xd0c0d0c,0xd0c0d0c,0xd0c0d0c,0xd0d0d0d,0xd0d0d0d,0xd0d0d0d,0xe0d0e0d,0xe0d0e0d,0xe0d0e0d,0xe0e0e0e,0xe0e0e0e,0xe0e0e0e,0xf0e0f0e,0xf0e0f0e,0xf0e0f0e,0xf0e0f0e,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0f0f0f,0xf0e0f0e,0xf0e0f0e,0xf0e0f0e,0xf0e0f0e,0xe0e0e0e,};
const int32_t sineLookupTable[] = {128 - 0x80, 128 - 0x83, 128 - 0x86, 128 - 0x89, 128 - 0x8c, 128 - 0x8f, 128 - 0x92, 128 - 0x95, 128 - 0x98, 128 - 0x9c, 128 - 0x9f, 128 - 0xa2, 128 - 0xa5, 128 - 0xa8, 128 - 0xab, 128 - 0xae, 128 - 0xb0, 128 - 0xb3, 128 - 0xb6, 128 - 0xb9, 128 - 0xbc, 128 - 0xbf, 128 - 0xc1, 128 - 0xc4, 128 - 0xc7, 128 - 0xc9, 128 - 0xcc, 128 - 0xce, 128 - 0xd1, 128 - 0xd3, 128 - 0xd5, 128 - 0xd8, 128 - 0xda, 128 - 0xdc, 128 - 0xde, 128 - 0xe0, 128 - 0xe2, 128 - 0xe4, 128 - 0xe6, 128 - 0xe8, 128 - 0xea, 128 - 0xeb, 128 - 0xed, 128 - 0xef, 128 - 0xf0, 128 - 0xf2, 128 - 0xf3, 128 - 0xf4, 128 - 0xf6, 128 - 0xf7, 128 - 0xf8, 128 - 0xf9, 128 - 0xfa, 128 - 0xfb, 128 - 0xfb, 128 - 0xfc, 128 - 0xfd, 128 - 0xfd, 128 - 0xfe, 128 - 0xfe, 128 - 0xfe, 128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xfe, 128 - 0xfe, 128 - 0xfd, 128 - 0xfd, 128 - 0xfc, 128 - 0xfc, 128 - 0xfb, 128 - 0xfa, 128 - 0xf9, 128 - 0xf8, 128 - 0xf7, 128 - 0xf6, 128 - 0xf5, 128 - 0xf4, 128 - 0xf2, 128 - 0xf1, 128 - 0xef, 128 - 0xee, 128 - 0xec, 128 - 0xeb, 128 - 0xe9, 128 - 0xe7, 128 - 0xe5, 128 - 0xe3, 128 - 0xe1, 128 - 0xdf, 128 - 0xdd, 128 - 0xdb, 128 - 0xd9, 128 - 0xd7, 128 - 0xd4, 128 - 0xd2, 128 - 0xcf, 128 - 0xcd, 128 - 0xca, 128 - 0xc8, 128 - 0xc5, 128 - 0xc3, 128 - 0xc0, 128 - 0xbd, 128 - 0xba, 128 - 0xb8, 128 - 0xb5, 128 - 0xb2, 128 - 0xaf, 128 - 0xac, 128 - 0xa9, 128 - 0xa6, 128 - 0xa3, 128 - 0xa0, 128 - 0x9d, 128 - 0x9a, 128 - 0x97, 128 - 0x94, 128 - 0x91, 128 - 0x8e, 128 - 0x8a, 128 - 0x87, 128 - 0x84, 128 - 0x81, 128 - 0x7e, 128 - 0x7b, 128 - 0x78, 128 - 0x75, 128 - 0x71, 128 - 0x6e, 128 - 0x6b, 128 - 0x68, 128 - 0x65, 128 - 0x62, 128 - 0x5f, 128 - 0x5c, 128 - 0x59, 128 - 0x56, 128 - 0x53, 128 - 0x50, 128 - 0x4d, 128 - 0x4a, 128 - 0x47, 128 - 0x45, 128 - 0x42, 128 - 0x3f, 128 - 0x3c, 128 - 0x3a, 128 - 0x37, 128 - 0x35, 128 - 0x32, 128 - 0x30, 128 - 0x2d, 128 - 0x2b, 128 - 0x28, 128 - 0x26, 128 - 0x24, 128 - 0x22, 128 - 0x20, 128 - 0x1e, 128 - 0x1c, 128 - 0x1a, 128 - 0x18, 128 - 0x16, 128 - 0x14, 128 - 0x13, 128 - 0x11, 128 - 0x10, 128 - 0x0e, 128 - 0x0d, 128 - 0x0b, 128 - 0x0a, 128 - 0x09, 128 - 0x08, 128 - 0x07, 128 - 0x06, 128 - 0x05, 128 - 0x04, 128 - 0x03, 128 - 0x03, 128 - 0x02, 128 - 0x02, 128 - 0x01, 128 - 0x01, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x01, 128 - 0x01, 128 - 0x01, 128 - 0x02, 128 - 0x02, 128 - 0x03, 128 - 0x04, 128 - 0x04, 128 - 0x05, 128 - 0x06, 128 - 0x07, 128 - 0x08, 128 - 0x09, 128 - 0x0b, 128 - 0x0c, 128 - 0x0d, 128 - 0x0f, 128 - 0x10, 128 - 0x12, 128 - 0x14, 128 - 0x15, 128 - 0x17, 128 - 0x19, 128 - 0x1b, 128 - 0x1d, 128 - 0x1f, 128 - 0x21, 128 - 0x23, 128 - 0x25, 128 - 0x27, 128 - 0x2a, 128 - 0x2c, 128 - 0x2e, 128 - 0x31, 128 - 0x33, 128 - 0x36, 128 - 0x38, 128 - 0x3b, 128 - 0x3e, 128 - 0x40, 128 - 0x43, 128 - 0x46, 128 - 0x49, 128 - 0x4c, 128 - 0x4f, 128 - 0x51, 128 - 0x54, 128 - 0x57, 128 - 0x5a, 128 - 0x5d, 128 - 0x60, 128 - 0x63, 128 - 0x67, 128 - 0x6a, 128 - 0x6d, 128 - 0x70, 128 - 0x73, 128 - 0x76, 128 - 0x79, 128 - 0x7c, 128 - 0x80,};
const int32_t cosLookupTable[] = {128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xff, 128 - 0xfe, 128 - 0xfe, 128 - 0xfd, 128 - 0xfd, 128 - 0xfc, 128 - 0xfc, 128 - 0xfb, 128 - 0xfa, 128 - 0xf9, 128 - 0xf8, 128 - 0xf7, 128 - 0xf6, 128 - 0xf5, 128 - 0xf4, 128 - 0xf2, 128 - 0xf1, 128 - 0xef, 128 - 0xee, 128 - 0xec, 128 - 0xeb, 128 - 0xe9, 128 - 0xe7, 128 - 0xe5, 128 - 0xe3, 128 - 0xe1, 128 - 0xdf, 128 - 0xdd, 128 - 0xdb, 128 - 0xd9, 128 - 0xd7, 128 - 0xd4, 128 - 0xd2, 128 - 0xcf, 128 - 0xcd, 128 - 0xca, 128 - 0xc8, 128 - 0xc5, 128 - 0xc3, 128 - 0xc0, 128 - 0xbd, 128 - 0xba, 128 - 0xb8, 128 - 0xb5, 128 - 0xb2, 128 - 0xaf, 128 - 0xac, 128 - 0xa9, 128 - 0xa6, 128 - 0xa3, 128 - 0xa0, 128 - 0x9d, 128 - 0x9a, 128 - 0x97, 128 - 0x94, 128 - 0x91, 128 - 0x8e, 128 - 0x8a, 128 - 0x87, 128 - 0x84, 128 - 0x81, 128 - 0x7e, 128 - 0x7b, 128 - 0x78, 128 - 0x75, 128 - 0x71, 128 - 0x6e, 128 - 0x6b, 128 - 0x68, 128 - 0x65, 128 - 0x62, 128 - 0x5f, 128 - 0x5c, 128 - 0x59, 128 - 0x56, 128 - 0x53, 128 - 0x50, 128 - 0x4d, 128 - 0x4a, 128 - 0x47, 128 - 0x45, 128 - 0x42, 128 - 0x3f, 128 - 0x3c, 128 - 0x3a, 128 - 0x37, 128 - 0x35, 128 - 0x32, 128 - 0x30, 128 - 0x2d, 128 - 0x2b, 128 - 0x28, 128 - 0x26, 128 - 0x24, 128 - 0x22, 128 - 0x20, 128 - 0x1e, 128 - 0x1c, 128 - 0x1a, 128 - 0x18, 128 - 0x16, 128 - 0x14, 128 - 0x13, 128 - 0x11, 128 - 0x10, 128 - 0x0e, 128 - 0x0d, 128 - 0x0b, 128 - 0x0a, 128 - 0x09, 128 - 0x08, 128 - 0x07, 128 - 0x06, 128 - 0x05, 128 - 0x04, 128 - 0x03, 128 - 0x03, 128 - 0x02, 128 - 0x02, 128 - 0x01, 128 - 0x01, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x00, 128 - 0x01, 128 - 0x01, 128 - 0x01, 128 - 0x02, 128 - 0x02, 128 - 0x03, 128 - 0x04, 128 - 0x04, 128 - 0x05, 128 - 0x06, 128 - 0x07, 128 - 0x08, 128 - 0x09, 128 - 0x0b, 128 - 0x0c, 128 - 0x0d, 128 - 0x0f, 128 - 0x10, 128 - 0x12, 128 - 0x14, 128 - 0x15, 128 - 0x17, 128 - 0x19, 128 - 0x1b, 128 - 0x1d, 128 - 0x1f, 128 - 0x21, 128 - 0x23, 128 - 0x25, 128 - 0x27, 128 - 0x2a, 128 - 0x2c, 128 - 0x2e, 128 - 0x31, 128 - 0x33, 128 - 0x36, 128 - 0x38, 128 - 0x3b, 128 - 0x3e, 128 - 0x40, 128 - 0x43, 128 - 0x46, 128 - 0x49, 128 - 0x4c, 128 - 0x4f, 128 - 0x51, 128 - 0x54, 128 - 0x57, 128 - 0x5a, 128 - 0x5d, 128 - 0x60, 128 - 0x63, 128 - 0x67, 128 - 0x6a, 128 - 0x6d, 128 - 0x70, 128 - 0x73, 128 - 0x76, 128 - 0x79, 128 - 0x7c, 128 - 0x80, 128 - 0x83, 128 - 0x86, 128 - 0x89, 128 - 0x8c, 128 - 0x8f, 128 - 0x92, 128 - 0x95, 128 - 0x98, 128 - 0x9c, 128 - 0x9f, 128 - 0xa2, 128 - 0xa5, 128 - 0xa8, 128 - 0xab, 128 - 0xae, 128 - 0xb0, 128 - 0xb3, 128 - 0xb6, 128 - 0xb9, 128 - 0xbc, 128 - 0xbf, 128 - 0xc1, 128 - 0xc4, 128 - 0xc7, 128 - 0xc9, 128 - 0xcc, 128 - 0xce, 128 - 0xd1, 128 - 0xd3, 128 - 0xd5, 128 - 0xd8, 128 - 0xda, 128 - 0xdc, 128 - 0xde, 128 - 0xe0, 128 - 0xe2, 128 - 0xe4, 128 - 0xe6, 128 - 0xe8, 128 - 0xea, 128 - 0xeb, 128 - 0xed, 128 - 0xef, 128 - 0xf0, 128 - 0xf2, 128 - 0xf3, 128 - 0xf4, 128 - 0xf6, 128 - 0xf7, 128 - 0xf8, 128 - 0xf9, 128 - 0xfa, 128 - 0xfb, 128 - 0xfb, 128 - 0xfc, 128 - 0xfd, 128 - 0xfd, 128 - 0xfe, 128 - 0xfe, 128 - 0xfe, 128 - 0xff, 128 - 0xff, 128 - 0xff,};
const u32 asciiFontTable[] = {9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 9984, 11008, 11264, 12288, 9984, 9984, 9984, 10240, 10496, 10752, 9984, 9984, 11520, 11776, 12032, 9728, 6656, 6912, 7168, 7424, 7680, 7936, 8192, 8448, 8704, 8960, 9216, 9984, 9984, 9984, 9984, 9472, 9984, 0, 256, 512, 768, 1024, 1280, 1536, 1792, 2048, 2304, 2560, 2816, 3072, 3328, 3584, 3840, 4096, 4352, 4608, 4864, 5120, 5376, 5632, 5888, 6144, 6400, 9984, 9984, 9984, 9984, 9984, 9984, 0, 256, 512, 768, 1024, 1280, 1536, 1792, 2048, 2304, 2560, 2816, 3072, 3328, 3584, 3840, 4096, 4352, 4608, 4864, 5120, 5376, 5632, 5888, 6144, 6400, 9984, 9984, 9984, 9984, 9984, };

const u8 scrollerText[] = "Hi! You're watching a short demo created with the help of ArchieSDK, a brand new Archimedes SDK! ArchieSDK uses GCC 8.5.0, allowing you to write RISC OS / Arthur software in C from the comfort of your desktop without having to mess with Acorn C/CPP! Make sure to check out the attached README for more information :)      Code by Targz, GFX by Targz and Tobach, Music by ToBach      Greetz to Bitshifters, Torment, Desire, Poo-Brain, TUHB, RiFT, Marquee Design, Field-FX, stardot, Hooy-Program, CSC, Logicoma, SVATG, Haujobb, SimonTime, Lex Bailey, Kitsu, Toad and You!!                                       ";

//global vars
u8* framebuffer;
u32 curFrame;
u32 curScrollerLetter;
bouncyBall* ballArray;
u8* moireGfx;

//code
void quit(){
    free(moireGfx);

    asm volatile("mov r0, #0\n"
                 "swi " swiToConst(QTM_Clear) "\n": : : "r0", "memory");

    v_disableVSync();

    v_waitForVSync(); //flush last vsync
}

void init(){ 
    v_setMode(13);
    v_disableTextCursor();
    v_enableVSync();

    atexit(quit); //register exit callback
}

//make sure that dest is aligned and size (in bytes) can be divided by 40; feel free to re-use this in other projects :)
void __attribute__((noinline)) memsetFast(u32* dest, u32 c, u32 size){
    asm volatile("push {%0, %1}\n"
        "mov r0, %1\n"
        "mov r1, %1\n"
        "mov r2, %1\n"
        "mov r3, %1\n"
        "mov r4, %1\n"
        "mov r5, %1\n"
        "mov r6, %1\n"
        "mov r7, %1\n"
        "mov r8, %1\n"
    "%=:\n" //auto generated label
        "stm %0!, {r0-r8,%1}\n"
        "cmp %0, %2\n"
        "bne %=b\n" //jump *b*ackwards to auto generated label
        "pop {%0, %1}\n"
    : : "r"(dest), "r"(c), "r"(dest + (size/4)) : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "cc", "memory");
}

void shiftScroller(u8* linestart){
    asm("push {%0}\n"
        ".rept 32\n"
        "ldm %0, {r0-r12}\n"
        "stm %0!, {r1-r12}\n"
        "ldm %0, {r0-r12}\n"
        "stm %0!, {r1-r12}\n"
        "ldm %0, {r0-r12}\n"
        "stm %0!, {r1-r12}\n"
        "ldm %0, {r0-r12}\n"
        "stm %0!, {r1-r12}\n"
        "ldm %0, {r0-r12}\n"
        "stm %0!, {r1-r12}\n"
        "ldm %0, {r0-r12}\n"
        "stm %0!, {r1-r12}\n"
        "ldm %0, {r0-r7}\n"
        // "mov r8, #0\n" //uncomment to clear up last 4 pixels
        "stm %0!, {r1-r8}\n"
        ".endr\n"
        "pop {%0}\n"
    : : "r"(linestart) : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "cc", "memory");
}

void drawMoire() {
    u32* framebufferStart = (u32*)(framebuffer + (48*320));
    u32* gfxPtr = (u32*)(&moireGfx[(80*640) + ((sineLookupTable[curFrame&255]>>1)*640)] + ((-120+(cosLookupTable[(curFrame+23)&0xff]>>2))<<2));
    u32* gfxPtr2 = (u32*)(&moireGfx[(80*640) + ((sineLookupTable[(curFrame+100)&255]>>1)*640)] + ((-120+(cosLookupTable[(curFrame)&0xff]>>2))<<2));
    u32 colour = 0;
    if((curFrame >= 1080) && (curFrame <= (1080+(5*256)))){

        colour = moireSineWithDither[(curFrame&255)>>1];
        u32 altColour = (colour<<8) | (colour>>24); 

        for(u32 i = 0; i < 160; ++i) {    
            asm volatile(".rept 16\n" //16*5 for a full line
                         "ldm %1!, {r0-r4}\n"
                         "ldm %2!, {r5-r9}\n"
                         "eor r0, r0, r5\n"
                         "orr r0, r0, %3\n"
                         "eor r1, r1, r6\n"
                         "orr r1, r1, %3\n"
                         "eor r2, r2, r7\n"
                         "orr r2, r2, %3\n"
                         "eor r3, r3, r8\n"
                         "orr r3, r3, %3\n"
                         "eor r4, r4, r9\n"
                         "orr r4, r4, %3\n"
                         "stm %0!, {r0-r4}\n"
                         ".endr\n"
                         "add %1, %1, #320\n"
                         "add %2, %2, #320\n"
                         : "+r"(framebufferStart), "+r"(gfxPtr), "+r"(gfxPtr2) 
                         : "r"((i&1)? altColour : colour) 
                         : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "cc", "memory");
        }

    } else {

        if(curFrame < 1080) colour = moireFadePalette[(curFrame-1032)>>2];
        else colour = moireFadePalette[11-((curFrame-(1080+(5*256)))>>2)];

        for(u32 i = 0; i < 160; ++i) {    
            asm volatile("orr %3, %3, lsl #8\n"
                         "orr %3, %3, lsl #16\n"
                         ".rept 16\n"
                         "ldm %1!, {r0-r4}\n"
                         "ldm %2!, {r5-r9}\n"
                         "eor r0, r0, r5\n"
                         "and r0, r0, %3\n"
                         "eor r1, r1, r6\n"
                         "and r1, r1, %3\n"
                         "eor r2, r2, r7\n"
                         "and r2, r2, %3\n"
                         "eor r3, r3, r8\n"
                         "and r3, r3, %3\n"
                         "eor r4, r4, r9\n"
                         "and r4, r4, %3\n"
                         "stm %0!, {r0-r4}\n"
                         ".endr\n"
                         "add %1, %1, #320\n"
                         "add %2, %2, #320\n"
                         : "+r"(framebufferStart), "+r"(gfxPtr), "+r"(gfxPtr2) 
                         : "r"(colour) 
                         : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "cc", "memory");
        }

    }
}

i32 randomBetween(i32 min, i32 max){
    return rand() % (max + 1 - min) + min;
}

void setupBouncyBalls(void){
    srand(0xB00B1E5); //Warning, this intro has boobies ;)

    ballArray = malloc(sizeof(bouncyBall)*nBalls);
    for(u32 i = 0; i < nBalls; ++i){
        bouncyBall* curBall = &(ballArray[i]);
        curBall->enabled = false;
        curBall->x = randomBetween(0, 303)&0xFFFFFFFA;
        curBall->y = randomBetween(130, 180);
        curBall->z = randomBetween(0, 255);

        curBall->velX = randomBetween(-4, 4);
        curBall->velY = randomBetween(0, 3);
        curBall->velZ = randomBetween(-4, 4);
        if(!curBall->velZ) curBall->velZ = randomBetween(-1,1) | 1;
        if(!curBall->velX) curBall->velX = (randomBetween(-1,1) | 1)<<1;
    }

}

void bouncyBallBackground(u32 backgroundPercent){
    if(backgroundPercent > 2) memsetFast((u32*)(framebuffer+((149)*320)), 0x01010101, 320);
    if(backgroundPercent > 3) memsetFast((u32*)(framebuffer+((150)*320)), 0x02020202, 320);
    if(backgroundPercent > 5) memsetFast((u32*)(framebuffer+((151)*320)), 0x29292929, 320*2);
    if(backgroundPercent > 8) memsetFast((u32*)(framebuffer+((153)*320)), 0x2a2a2a2a, 320*3);
    if(backgroundPercent > 11) memsetFast((u32*)(framebuffer+((156)*320)), 0x2b2b2b2b, 320*3);
    if(backgroundPercent > 14) memsetFast((u32*)(framebuffer+((159)*320)), 0xc4c4c4c4, 320*4);
    if(backgroundPercent > 19) memsetFast((u32*)(framebuffer+((163)*320)), 0xc2c2c2c2, 320*5);
    if(backgroundPercent > 24) memsetFast((u32*)(framebuffer+((168)*320)), 0xc3c3c3c3, 320*7);
    if(backgroundPercent > 31) memsetFast((u32*)(framebuffer+((175)*320)), 0xECECECEC, 320*8);
    if(backgroundPercent > 39) memsetFast((u32*)(framebuffer+((183)*320)), 0xEAEAEAEA, 320*10);
    if(backgroundPercent > 49) memsetFast((u32*)(framebuffer+((193)*320)), 0xEBEBEBEB, 320*15);
}

void updateBouncyBalls(bool killBalls){
    //v_setBorderColourRGB(0xFF,0xFF,0xFF);
    u8* middle = framebuffer + 160;

    //draw first, then update
    if(!curFrame) goto skipDraw; //don't draw on the first frame
    for(u32 i = 0; i < nBalls; ++i){ //draw shadows
        register bouncyBall* curBall = &(ballArray[i]);
        if(!curBall->enabled) continue;

        u32 ball = (curBall->z>>6) + ((((192-curBall->y)>>5))); 
        if(ball>3) ball = 3;
        asm(".rept 8\n"
            "ldm %0, {r0-r3}\n"
            "ldm %1, {r4-r7}\n"
            "add %1, #64\n"
            "ldm %2!, {r8}\n"
            "bic r0, r0, r4\n"
            "bic r1, r1, r5\n"
            "bic r2, r2, r6\n"
            "bic r3, r3, r7\n"
            "and r9, r4, r8\n"
            "orr r0, r0, r9\n"
            "and r9, r5, r8\n"
            "orr r1, r1, r9\n"
            "and r9, r6, r8\n"
            "orr r2, r2, r9\n"
            "and r9, r7, r8\n"
            "orr r3, r3, r9\n"
            "stm %0, {r0-r3}\n"
            "add %0, #320\n"
            ".endr\n"
            :
            : "r"(ALIGN_PTR_DOWN(middle + ((((200<<11)/(curBall->z+1120))>>1)*320) + (((curBall->x<<11)/(curBall->z+1120))>>1))), "r"(ballGfxArray[ball]+16), "r"(&ballShadowPalette[(((200<<11)/(curBall->z+1120))>>1)-148])
            : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "cc", "memory");
    }
    //draw balls
    for(u32 c = 3; c != UINT32_MAX; --c) {
        const u8* curBallGfx = ballGfxArray[c];
        for(u32 i = 0; i < nBalls; ++i){
            register bouncyBall* curBall = &(ballArray[i]);
            if(((curBall->z>>6)&3) != c) continue;
            if(!curBall->enabled) continue;
            
            asm volatile("push {%1}\n"
                         ".rept 16\n"
                         "ldm %0, {r0-r3}\n"
                         "ldm %1!, {r4-r11}\n"
                         "bic r0, r0, r8\n"
                         "bic r1, r1, r9\n"
                         "bic r2, r2, r10\n"
                         "bic r3, r3, r11\n"
                         "orr r0, r0, r4\n"
                         "orr r1, r1, r5\n"
                         "orr r2, r2, r6\n"
                         "orr r3, r3, r7\n"
                         "stm %0, {r0-r3}\n"
                         "add %0, #320\n"
                         ".endr\n"
                         "pop {%1}\n"
                         :
                         : "r"(ALIGN_PTR_DOWN(middle + ((((curBall->y<<11)/(curBall->z+1120))>>1)*320) + (((curBall->x<<11)/(curBall->z+1120))>>1))), "r"(curBallGfx)
                         : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "cc", "memory");
        }
    }

    skipDraw:
    //v_setBorderColourRGB(0xFF,0,0xFF);
    //update balls
    for(u32 i = 0; i < nBalls; ++i){
        bouncyBall* curBall = &(ballArray[i]);
        if(!curBall->enabled && curFrame) continue;

        curBall->x += curBall->velX;
        if(curFrame&1) ++curBall->velY;
        curBall->y += curBall->velY;
        curBall->z += curBall->velZ;

        if(curBall->x < -159) {
            curBall->velX = -curBall->velX;
            curBall->x = -159 - (curBall->x - -159);
            if(killBalls) curBall->enabled = false;
        } else if(curBall->x > 144) {
            curBall->velX = -curBall->velX;
            curBall->x = 144 - (curBall->x - 144);
            if(killBalls) curBall->enabled = false;
        }

        if(curBall->y > (191)) {
            curBall->velY = -curBall->velY;
            curBall->y = (191) - (curBall->y - (191));
            if(curBall->velY < -10) curBall->velY += 2;
            if(killBalls) curBall->enabled = false;
        }

        if(curBall->z < 1) {
            curBall->velZ = -curBall->velZ;
            curBall->z = 1 - (curBall->z -1);
            if(killBalls) curBall->enabled = false;
        } else if(curBall->z > 255) {
            curBall->velZ = -curBall->velZ;
            curBall->z = 255 - (curBall->z - 255);
            if(killBalls) curBall->enabled = false;
        }
    }
}

void plotPoints(register u8* const framebufferPos, register const point_t* points, register u32 nPoints, register const u8 colour){
    register const int32_t curCos = cosLookupTable[curFrame&255];
    register const int32_t curSin = sineLookupTable[curFrame&255];
    while(nPoints--) {
        *(framebufferPos + (points->y) + ((((points->x*curCos)) + ((points->z*curSin)))>>7)) = colour;
        ++points;
    }
}

u32 isqrt(u32 y) {
	u32 L = 0;
	u32 M;
	u32 R = y + 1;

    while (L != R - 1)
    {
        M = (L + R) / 2;

		if (M * M <= y)
			L = M;
		else
			R = M;
	}

    return L;
}

int main(){

    curFrame = 0;
    curScrollerLetter = 0;

    //load music
    puts("Loading music...");
    asm volatile("mov r0, %0\n"
                 "mov r1, #0\n"
                 "swi " swiToConst(QTM_Load)"\n"
                 : 
                 : "r"("<Demo$Dir>.Music")
                 : "r0", "r1", "cc", "memory");

    puts("Pre-calculating...");

    moireGfx = malloc(320*640);

    u32* squaredDY = malloc(sizeof(u32) * 320);
    u32* y640 = malloc(sizeof(u32) * 320);
    for(int y = 0; y < 320; ++y) {
        squaredDY[y] = (y - 160) * (y - 160);
        y640[y] = y*640;
    }

    for(int x = 0; x < 640; ++x) {
        u32 dx = (x - 320) * (x - 320);
        for(int y = 0; y < 320; ++y) {
            moireGfx[y640[y]+x] = (!((((u32)isqrt(dx + squaredDY[y])) >> 4) & 1))? 255: 0;
        }
    }

    free(squaredDY);
    free(y640);

    init();

    framebuffer = v_getScreenAddress();

    setupBouncyBalls();
    updateBouncyBalls(false); //update ball positions once

    memsetFast((u32*)(framebuffer), 0, 256*320); //clear screen

    memcpy(framebuffer+640, slpbuggy_logo, slpbuggy_logo_size);

    //draw little seperator bars
    memsetFast((u32*)(framebuffer+(44*320)), 0x2d2d2d2d, 320);
    memsetFast((u32*)(framebuffer+(45*320)), 0x2f2f2f2f, 320);
    memsetFast((u32*)(framebuffer+(46*320)), 0x2c2c2c2c, 320);
    memsetFast((u32*)(framebuffer+(47*320)), 0x02020202, 320);
    memsetFast((u32*)(framebuffer+(208*320)), 0x2d2d2d2d, 320);
    memsetFast((u32*)(framebuffer+(209*320)), 0x2f2f2f2f, 320);
    memsetFast((u32*)(framebuffer+(210*320)), 0x2c2c2c2c, 320);
    memsetFast((u32*)(framebuffer+(211*320)), 0x02020202, 320);
    memsetFast((u32*)(framebuffer+(252*320)), 0x2d2d2d2d, 320);
    memsetFast((u32*)(framebuffer+(253*320)), 0x2f2f2f2f, 320);
    memsetFast((u32*)(framebuffer+(254*320)), 0x2c2c2c2c, 320);
    memsetFast((u32*)(framebuffer+(255*320)), 0x02020202, 320);

    v_waitForVSync(); //wait a frame
    
    asm volatile("swi " swiToConst(QTM_Start) "\n": : : "memory", "cc");

    while(!k_checkKeypress(KEY_ESCAPE)){

        //v_setBorderColourRGB(0x00,0x00,0xff);
        //draw effects in middle of screen
        
        if(curFrame < 1032) {
            memsetFast((u32*)(framebuffer+(48*320)), 0, 160*320); //clear middle part of screen (line 48 to line 208)

            //where to plot the points
            u8* plotPosition = (framebuffer + ((128*320)+160)) + (sineLookupTable[curFrame&255]) + ((cosLookupTable[((curFrame+23)<<1)&255]>>3)*320);

            if(curFrame < 64) plotPoints(plotPosition, acornVertices, (u32)(((float)(curFrame) / 64.0f) * (float)acornVerticesLen), acornColour);
            else if(curFrame < 341 - 64) plotPoints(plotPosition, acornVertices, acornVerticesLen, acornColour);
            else if(curFrame < (341)) plotPoints(plotPosition, acornVertices, (u32)((1.0f-((float)(curFrame - (341-64)) / 64.0f)) * (float)acornVerticesLen), acornColour);
            else if(curFrame < 341+64) plotPoints(plotPosition, cVertices, (u32)(((float)(curFrame - 341) / 64.0f) * (float)cVerticesLen), cColour);
            else if(curFrame < (642-64)) plotPoints(plotPosition, cVertices, cVerticesLen, cColour);
            else if(curFrame < 642) plotPoints(plotPosition, cVertices, (u32)((1.0f-((float)(curFrame - (642-64)) / 64.0f)) * (float)cVerticesLen), cColour);
            else if(curFrame < 642 + 64) plotPoints(plotPosition, owl4xVertices, (u32)(((float)(curFrame - 642) / 64.0f) * (float)owl4xVerticesLen), owl4xColour);
            else if(curFrame < 1023 - 64) plotPoints(plotPosition, owl4xVertices, owl4xVerticesLen, owl4xColour);
            else plotPoints(plotPosition, owl4xVertices, (u32)((1.0f-((float)(curFrame - (1023-64)) / 64.0f)) * (float)owl4xVerticesLen), owl4xColour);

        } else if(curFrame < 1080 + (5*256)+48){
            drawMoire();
        
        } else if(curFrame < 1080 + (5*256)+48 + 64){
            
            //draw the background
            bouncyBallBackground(curFrame - (1080 + (5*256)+48));

        } else if(curFrame < 1080 + (5*256)+48 + 64 + 768){
            
            //add balls
            if(curFrame < 1080 + (5*256)+48 + 64 + (nBalls<<4)) {
                if((curFrame&0b1111) == 0) ballArray[(curFrame-(1080 + (5*256)+48 + 64))>>4].enabled = true;
            }

            //draw background & balls
            memsetFast((u32*)(framebuffer+(48*320)), 0, 101*320);
            bouncyBallBackground(64);
            updateBouncyBalls(curFrame > 1080 + (5*256)+48 + 64 + 768 - 32);
        
        } else {
            
            //remove background
            memsetFast((u32*)(framebuffer+(48*320)), 0, 160*320);
            bouncyBallBackground(64 - (curFrame - (1080 + (5*256)+48 + 768 + 64)));

        }

        //v_setBorderColourRGB(0xff,0x00,0x00);
        //shift scrolling part of screen by 4 pixels
        shiftScroller(framebuffer+(216*320)); 

        //write character slice
        //v_setBorderColourRGB(0x00,0xFF,0x00);
        u32* framebufferSliceStart = &((u32*)framebuffer)[(217*(320/4))-1];
        u32* letterSlice = &fontData[asciiFontTable[scrollerText[curScrollerLetter]]]  + ((curFrame&7)<<5) - 1;
        for(u32 i = 0; i < 32; ++i){
            *(framebufferSliceStart) = *(++letterSlice);
            framebufferSliceStart+=80;
        }
        if((curFrame&7) == 7) ++curScrollerLetter;
        if(curScrollerLetter >= sizeof(scrollerText)) curScrollerLetter = 0;
        
        //v_setBorderColourRGB(0x00,0x00,0x00);
        //vsync
        v_waitForVSync();

        ++curFrame;
        if(curFrame == (1080 + (5*256)+48) + 64 + 768 + 64) curFrame = 0; //all done? start over
    }

    free(ballArray);

	return 0;
}
