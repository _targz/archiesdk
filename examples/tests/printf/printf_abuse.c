#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// This program is a test app for printf. It was used to compare my printf 
// implementation against GLIBC's on my PC. As such, this program compiles and
// runs on both RISC OS/Arthur and Linux. 

#ifdef __archie__

#include "archie/keyboard.h"
#include "archie/utils.h"
#include "archie/video.h"

#define NEWLINE "\r\n"

void quit(){
	puts("atexit_func called!");
	v_disableVSync();
	//flush last vsync
	v_waitForVSync();
}

#else

#include "stdint.h"
#define NEWLINE "\n"
typedef uint32_t u32;
typedef int32_t i32;
void p_clearConsole(void){
	system("clear");
}

#endif

void init(){ 
#ifdef __archie__
    v_setMode(13);
    v_disableTextCursor();
    v_enableVSync();
    atexit(quit);
#endif
}

u32 tmpcnt = 0;
#define printt(fmt, ...) if((tmpcnt&0xf) == 0) p_clearConsole(); printf("format:"); puts(fmt); printf("Result: "fmt NEWLINE, ##__VA_ARGS__); ++tmpcnt; if((tmpcnt&0xf) == 0xf) waitForNewChar()

void waitForNewChar(void){
#ifdef __archie__
    puts("Press any key...");
    char key = getchar();
    while(k_checkKeypress(key)) v_waitForVSync();
#else
    printf("Press Enter...\n");
    getchar();
#endif
}

int main(){
    
    init();
    tmpcnt = 0;
    printf("f:%f; i:%i %%%% c:%c s:%s\r\n%%.0f:%.0f %%.f:%.f %%.7f:%.0007f %%.2f:%.2f\r\n", (double)-123.456f, (signed int)-123, '$', "bonjour", (double)123.456f, (double)4.2f, (double)1.00001f, (double)45.678f);
    waitForNewChar();
    printt("% 8.3f.", (double)1.1f);
    printt("%- 8.3f.", (double)1.1f);
    printt("% 6.3d.", 1);
    printt("%- 6.9d.", 1);
    printt("% 8.3f.", (double)-1.1f);
    printt("%- 8.3f.", (double)-1.1f);
    printt("% 6.9d.", -1);
    printt("%- 6.9d.", -1);
    printt("% 8.3f.", (double)165423456.165423456f);
    printt("%- 8.3f.", (double)165423456.165423456f);
    printt("% 6.3d.", 125563185);
    printt("%- 6.3d.", 165423456);
    printt("% 8.3f.", (double)-165423456.165423456f);
    printt("%- 8.3f.", (double)-165423456.165423456f);
    printt("% 6.9d.", -165423456);
    printt("%- 6.9d.", -165423456);
    printt("No args");
	printt("character: %c.", 'd');
	printt("str: %s.", "Hello");
	printt("ptr: %p.", "aaa");
	printt("signed integer: %d.", -5);
	printt("signed integer: %i.", -5);
	printt("unsigned integer: %u.", -5);
	printt("hex: %x.", 0xffffffff);
	printt("hex uppercase: %X.", 0xffffffff);
	printt("%% character.");
	printt("Blank padding: %3d.", 3);
	printt("0pad: %03d.", 700);
	printt("neg width pad: %-3d.", 7);
	printt("width&character: %5c.", 'c');
	printt("width&str: %5s.", "Hi");
	printt("width&int: %5d.", 5);
	printt("width&int (2): %5i.", 5);
	printt("width&ptr: %16p.", "");
	printt("width&uint: %5u.", 50);
	printt("width&lowerhex: %10x.", 0xffffffff);
	printt("width&upperhex: %10X.", 0xffffffff);
	printt("str prec (1.9): %1.9s.", "Hello");
	printt("str prec (2.1): %2.1s.", "Hello");
	printt("str prec (5.1): %5.1s.", "Hello");
	printt("str prec (5.3): %5.3s.", "Hello");
	printt("str prec (4.4): %4.4s.", "Hello");
	printt("str prec (5.4): %5.4s.", "Hello");
	printt("str prec (9.2): %9.2s.", "Hello");
	printt("str prec (5.2): %5.2s.", "Hello");
	printt("lpad str prec (1.9): %-1.9s.", "Hello");
	printt("lpad str prec (2.1): %-2.1s.", "Hello");
	printt("lpad str prec (5.1): %-5.1s.", "Hello");
	printt("lpad str prec (5.3): %-5.3s.", "Hello");
	printt("lpad str prec (4.4): %-4.4s.", "Hello");
	printt("lpad str prec (5.4): %-5.4s.", "Hello");
	printt("lpad str prec (9.2): %-9.2s.", "Hello");
	printt("lpad str prec (5.2): %-5.2s.", "Hello");
	printt("Int prec: %.5d.", -5);
	printt("Int prec (2): %.2i.", 500);
	printt("Uint prec: %.5d.", 5);
	printt("Hexa prec: %.9x.", 0xdeadbeef);
	printt("Hexa prec (2): %.9X.", 0xdeadbeef);
	printt("Int prec&pad (10.5): %10.5d.", 5);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
	printt("Int prec&0pad (10.5): %010.5d.", 5); //yes i know this isn't valid, GCC
#pragma GCC diagnostic pop
	printt("Int prec&pad (4.2): %4.2d.", 500);
	printt("Int prec&pad (5.10): %5.10d.", 5);
	printt("Int prec&neg pad (10.5): %-10.5d.", 5);
	printt("Int pad width&*: %*d.", 5, 18);
	printt("Int pad&prec&*: %*.*d.", 10, 5, 18);
    printt("double 1.0: %f.", 1.0);
	printt("double 2.0: %f.", 3.0);
	printt("double 3.0e2: %f.", 3.0e2);
	printt("double -3.0e3: %f.", -3.0e3);
	printt("double 3.0e4: %f.", 3.0e4);
	printt("double 1.002: %f.", 1.002);
	printt("double -3.005: %f.", -3.005);
	printt("double 3.000005: %f.", 3.000005);
	printt("double -3.0000005: %f.", -3.0000005);
	printt("double -3.0000004: %f.", -3.0000004);
	printt("double 3.0000006: %f.", 3.0000006);
	printt("double -0.55: %f.", -0.55);
	printt("double 3.5e3: %f.", 3.5e3);
	printt("double&prec 0: %.0f.", 45654.245);
	printt("double&prec 1: %.1f.", 45654.245);
	printt("double&prec 2: %.2f.", 45654.245);
	printt("double&prec 9: %.9f.", 45654.245);
	printt("double&width&prec: %5.2f.", 5.5);
	printt("double&neg width&prec: %-5.2f.", 5.5);
	printt("double&NaN: %f.", 0.0/0.0);
	printt("double&Inf: %f.", 1.0/0.0);
	printt("double&-Inf: %f.", -1.0/0.0);
	printt("double&NaN&pad: %9f.", 0.0/0.0);
	printt("double&NaN&neg pad: %-9f.", 0.0/0.0);

    return 0;
}
