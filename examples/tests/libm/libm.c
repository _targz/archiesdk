#include "stdio.h"
#include "stdlib.h"
#include "math.h"

// This program is a test app for libm. It was used to compare my libm 
// implementation against GLIBC's on my PC. As such, this program compiles and
// runs on both RISC OS/Arthur and Linux. 

#ifdef __archie__
#include "archie/keyboard.h"
#include "archie/utils.h"
#include "archie/video.h"

void quit(){
    printf("atexit_func called!\r\n");
    v_disableVSync();
    //flush last vsync
    v_waitForVSync();
}
#else
#include <stdint.h>
typedef uint32_t u32;
typedef int32_t i32;
void p_clearConsole(void){
    system("clear");
}
#endif

void init(){ 
#ifdef __archie__
    v_setMode(13);
    v_disableTextCursor();
    v_enableVSync();
    atexit(quit);
#endif
}

float bigfloats[24] = {
    //few smalls
    0.789f,
    -0.789f,
    0.999f,
    -0.999f,
    1.001f,
    -1.001f,
    0.001f,
    -0.001f,
    1.0f,
    -1.0f,
    0.450f,
    -0.450f,
    0.510f,
    -0.510f,
    //bigs
    1234.56789f,
    -1234.56789f,
    34848516.123f,
    -34848516.123f,
    //error test
    0.0f,
    -0.0f,
    INFINITY,
    -INFINITY,
    NAN,
    -NAN
};

float smallfloats[20] = {
    //smalls
    0.0f,
    1.0f,
    0.999f,
    -0.999f,
    1.000001f,
    -1.000001f,
    0.123f,
    0.5f,
    0.789f,
    -0.0f,
    -1.0f,
    -0.123f,
    -0.5f,
    -0.789f,
    //couple of bigs
    1234.0f,
    -1234.0f,
    //error test
    INFINITY,
    NAN,
    -INFINITY,
    -NAN
};

// test macros
#define testfuncsmall(func) p_clearConsole(); for(u32 i = 0; i < 20; ++i) printf("%s(%f) = %f\r\n", #func, (double)smallfloats[i], (double)func(smallfloats[i])); waitForNewChar()
#define testfuncbig(func) p_clearConsole(); for(u32 i = 0; i < 24; ++i) printf("%s(%f) = %f\r\n", #func, (double)bigfloats[i], (double)func(bigfloats[i])); waitForNewChar()
#define testfunccustom(call) printf("%s = %f\r\n", #call, (double)call);

void waitForNewChar(void){
#ifdef __archie__
    puts("Press any key...");
    char key = getchar();
    while(k_checkKeypress(key)) v_waitForVSync();
#else
    printf("Press Enter...\n");
    getchar();
#endif
}
int main(){
    
    init();

    //smalls
    testfuncsmall(cos);
    testfuncsmall(sin);
    testfuncsmall(tan);
    testfuncsmall(acos);
    testfuncsmall(asin);
    testfuncsmall(atan);
    testfuncsmall(cosh);
    testfuncsmall(sinh);
    testfuncsmall(tanh);

    // //bigs
    testfuncbig(exp);
    testfuncbig(log);
    testfuncbig(log10);
    testfuncbig(sqrt);
    testfuncbig(ceil);
    testfuncbig(fabs);
    testfuncbig(floor);

    //custom tests
    p_clearConsole(); 
    testfunccustom(ldexp(7.0f, -4));
    testfunccustom(ldexp(7.0f, 3));
    testfunccustom(ldexp(M_PI, -10));
    testfunccustom(ldexp(M_PI, 0));
    testfunccustom(ldexp(0.0f, -10));
    testfunccustom(ldexp(INFINITY, 6));
    testfunccustom(ldexp(NAN, 6));
    testfunccustom(ldexp(-INFINITY, 6));
    testfunccustom(ldexp(-NAN, 6));
    waitForNewChar();
    
    p_clearConsole(); 
    #define powd(arg1, arg2) testfunccustom(pow(arg1, arg2))
    powd(7.0f, -4.0f);
    powd(-7.0f, 3.0f);
    powd(-7.0f, 2.0f);
    powd(5.0f, 2.0f);
    powd(0, 6.0f);
    powd(1, 6.0f);
    powd(M_PI, 0.5f);
    powd(M_PI, 0.0f);
    powd(0.0f, -10.0f);
    powd(INFINITY, 6.0f);
    powd(NAN, 6.0f);
    powd(-INFINITY, 6.0f);
    powd(-NAN, 6.0f);
    powd(-6.0f, 3.0f);
    powd(-6.0f, 3.1f);
    powd(-6.0f, 2.0f);
    powd(-6.0f, 2.1f);
    powd(-6.0f, 2.05f);
    powd(6.0f, 2.05f);
    powd(-6.0f, -3.0f);
    powd(-6.0f, -3.1f);
    powd(-6.0f, -2.0f);
    powd(-6.0f, -2.1f);
    powd(-6.0f, -2.05f);
    powd(6.0f, -2.05f);
    powd(6.0f, -3.0f);
    powd(6.0f, -3.1f);
    powd(6.0f, -2.0f);
    powd(6.0f, -2.1f);
    powd(6.0f, -2.05f);
    waitForNewChar();
    
    p_clearConsole(); 
    testfunccustom(fmod(5.1f, 3.0f));
    testfunccustom(fmod(7.0f, 3.0f));
    testfunccustom(fmod(-5.1f, 3.0f));
    testfunccustom(fmod(5.1f, -3.0f));
    testfunccustom(fmod(-5.1f, -3.0f));
    testfunccustom(fmod(0.0f, 1.0f));
    testfunccustom(fmod(-0.0f, 1.0f));
    testfunccustom(fmod(INFINITY, 6.0f));
    testfunccustom(fmod(NAN, 6.0f));
    testfunccustom(fmod(-INFINITY, 6.0f));
    testfunccustom(fmod(-NAN, 6.0f));
    waitForNewChar();

    return 0;
}