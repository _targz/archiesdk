#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <swis.h>

#include "archie/video.h"
#include "archie/keyboard.h"


void quit(){
    v_disableVSync();
    //flush last vsync
    v_waitForVSync();
}

void init(){ 
    v_setMode(13);
    v_disableTextCursor();
    v_enableVSync();

    atexit(quit); //register exit callback
}

int main(int argc, char* argv[]){
    
    init();
    
    printf("Hellorld! This is a basic C program\r\ncompiled with ArchieSDK!\r\n\r\nCommand line: ");
    for(int i = 0; i < argc; ++i) printf("%s ", argv[i]);

    time_t curtime = time(NULL);
    printf("\r\nToday is %s\r\n\r\n", ctime(&curtime));

    printf("Here's the RISC OS char table\r\n\r\n    0 1 2 3 4 5 6 7 8 9 A B C D E F");
    for(u32 i = 0; i < 256; ++i) {
        if((i&0b1111) == 0) printf("\r\n%02lX: ", i);
        printf("%c ", isprint(i)? (char)i : ' ');
    }

	return 0;
}